/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  AppDelegate.h
 项目：  SuperTeacher
 时间：  16/1/27
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import <UIKit/UIKit.h>
@class IIViewDeckController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) IIViewDeckController* deckController;

@end
