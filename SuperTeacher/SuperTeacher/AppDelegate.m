/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  AppDelegate.m
 项目：  SuperTeacher
 时间：  16/1/27
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：
 */

/**
 * 资源库 -> Developer -> Xcode -> UserData -> CodeSnippets
 */

#import "AppDelegate.h"
#import "IIViewDeckController.h"
#import "CViewController.h"
#import "LeftViewController.h"
#import "GuideViewController.h"

@interface AppDelegate () <IIViewDeckControllerDelegate>

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    
    if ([[LoginHelper getUserID] length])
    {
        LeftViewController *leftVC = [[LeftViewController alloc] init];
        CViewController *cVC = [[CViewController alloc] init];
        
        IIViewDeckController* deckController =  [[IIViewDeckController alloc] initWithCenterViewController:[[UINavigationController alloc] initWithRootViewController:cVC] leftViewController:leftVC];
        self.deckController = deckController;
        deckController.delegate = self;
        deckController.leftSize = [[UIScreen mainScreen] bounds].size.width - 275.0f;
        deckController.openSlideAnimationDuration = 0.3f;
        deckController.closeSlideAnimationDuration = 0.3f;
        deckController.bounceDurationFactor = 0.3f;
        deckController.centerhiddenInteractivity = IIViewDeckCenterHiddenNotUserInteractiveWithTapToClose;
        self.window.rootViewController = deckController;
    }
    else
    {
        self.window.rootViewController = [[GuideViewController alloc] init];
    }
    
    [self.window makeKeyAndVisible];
    return YES;
}

#pragma mark - IIViewDeckControllerDelegate
- (void)viewDeckController:(IIViewDeckController*)viewDeckController applyShadow:(CALayer*)shadowLayer withBounds:(CGRect)rect
{
    shadowLayer.masksToBounds = NO;
    shadowLayer.shadowRadius = 5;
    shadowLayer.shadowOpacity = 0.9;
    shadowLayer.shadowColor = [[UIColor blackColor] CGColor];
    shadowLayer.shadowOffset = CGSizeZero;
    shadowLayer.shadowPath = [[UIBezierPath bezierPathWithRect:rect] CGPath];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
