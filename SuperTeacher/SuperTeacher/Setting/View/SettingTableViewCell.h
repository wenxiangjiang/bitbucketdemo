/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  SettingTableViewCell.h
 项目：  SuperTeacher
 时间：  16/1/21
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import <UIKit/UIKit.h>

@interface SettingTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIView *lineView;

@end
