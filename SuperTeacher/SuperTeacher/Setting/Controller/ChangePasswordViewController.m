/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  ChangePasswordViewController.m
 项目：  SuperTeacher
 时间：  16/1/21
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import "ChangePasswordViewController.h"
#import "AppDelegate.h"
#import "GuideViewController.h"

@interface ChangePasswordViewController ()

@property (nonatomic, strong) UITextField *firstTextField;
@property (nonatomic, strong) UITextField *secondTextField;
@property (nonatomic, strong) UIView *firstLineView;
@property (nonatomic, strong) UIView *secondLineView;
@property (nonatomic, strong) UIButton *showButton;
@property (nonatomic, strong) UIButton *submitButton;

@end

@implementation ChangePasswordViewController

#pragma mark - Lifecycle
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"修改密码";
    
    [self.view addSubview:self.firstTextField];
    [self.view addSubview:self.secondTextField];
    [self.view addSubview:self.firstLineView];
    [self.view addSubview:self.secondLineView];
    [self.view addSubview:self.showButton];
    [self.view addSubview:self.submitButton];
}

#pragma mark - UITableViewDataSource
#pragma mark - UITableViewDelegate
#pragma mark - CustomDelegate
#pragma mark - Event and Responce
- (void)buttonClicked:(UIButton *)button
{
    LPMethod();
    switch (button.tag)
    {
        case 1:
        {
            if (button.selected)
            {
                self.secondTextField.secureTextEntry = YES;
            }
            else
            {
                self.secondTextField.secureTextEntry = NO;
            }
            
            // 解决secureTextEntry在切换过程中导致光标错位的问题
            NSString *tempString = self.secondTextField.text;
            self.secondTextField.text = @"";
            self.secondTextField.text = tempString;
            
            button.selected = !button.selected;
        }
            break;
        case 2:
        {
            LPLog(@"确认修改");
            [self.firstTextField resignFirstResponder];
            [self.secondTextField resignFirstResponder];
            
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            [params setObject:[LoginHelper getUserID] forKey:@"uid"];
            [params setObject:self.firstTextField.text forKey:@"oldpassword"];
            [params setObject:self.secondTextField.text forKey:@"password"];
            
            [Api requestWithMethod:@"POST" path:API_CHANGEPASSWORD_URL params:params success:^(id responseObject) {
                LPLog(@"%@", responseObject);
                if ([responseObject[@"code"] intValue] == 0)
                {
                    [LoginHelper saveUserName:@""];
                    [LoginHelper saveUserID:@""];
                    [LoginHelper saveRealName:@""];
                    [LoginHelper savePhotoUrl:@""];
                    [LoginHelper saveClassStatus:@""];
                    
                    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    appDelegate.window.rootViewController = [[GuideViewController alloc] init];
                    [appDelegate.window makeKeyAndVisible];
                }
                else
                {
                    [ProgressHelper showTextHUDAddedTo:self.view text:responseObject[@"msg"] animated:YES];
                }
            } failed:^(NSError *error) {
                [ProgressHelper showTextHUDAddedTo:self.view text:[error localizedDescription] animated:YES];
            }];

        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Notification Methods
#pragma mark - Private Methods
#pragma mark - Setters and Getters
- (UITextField *)firstTextField
{
    if (!_firstTextField)
    {
        _firstTextField = [[UITextField alloc] initWithFrame:CGRectMake(20, 88, [[UIScreen mainScreen] bounds].size.width-40, 48)];
        _firstTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"请输入旧密码" attributes:@{NSForegroundColorAttributeName: LPHEX(0xcacaca)}];
        _firstTextField.font = [UIFont systemFontOfSize:15.0f];
        _firstTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        _firstTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _firstTextField.keyboardType = UIKeyboardTypeASCIICapable;
        _firstTextField.tintColor = LPDefaultColorNormal;
        
    }
    return _firstTextField;
}

- (UITextField *)secondTextField
{
    if (!_secondTextField)
    {
        _secondTextField = [[UITextField alloc] initWithFrame:CGRectMake(20, 145, [[UIScreen mainScreen] bounds].size.width-90, 48)];
        _secondTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"请输入新密码" attributes:@{NSForegroundColorAttributeName: LPHEX(0xcacaca)}];
        _secondTextField.font = [UIFont systemFontOfSize:15.0f];
        _secondTextField.secureTextEntry = YES;
        _secondTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        _secondTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _secondTextField.tintColor = LPDefaultColorNormal;
    }
    return _secondTextField;
}

- (UIView *)firstLineView
{
    if (!_firstLineView)
    {
        _firstLineView = [[UIView alloc] initWithFrame:CGRectMake(20, 136, [[UIScreen mainScreen] bounds].size.width-40, LPSeparatorHeight)];
        _firstLineView.backgroundColor = LPSeparatorColor;
    }
    return _firstLineView;
}

- (UIView *)secondLineView
{
    if (!_secondLineView)
    {
        _secondLineView = [[UIView alloc] initWithFrame:CGRectMake(20, 193, [[UIScreen mainScreen] bounds].size.width-40, LPSeparatorHeight)];
        _secondLineView.backgroundColor = LPSeparatorColor;
    }
    return _secondLineView;
}

- (UIButton *)showButton
{
    if (!_showButton)
    {
        _showButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _showButton.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width-60, 159, 40, 20);
        _showButton.layer.masksToBounds = YES;
        _showButton.layer.cornerRadius = 5;
        _showButton.titleLabel.font = [UIFont systemFontOfSize:15.0f];
        _showButton.tag = 1;
        [_showButton setTitle:@"显示" forState:UIControlStateNormal];
        [_showButton setTitle:@"隐藏" forState:UIControlStateSelected];
        [_showButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_showButton setBackgroundImage:[[LPHelper shareLPHelper] imageWithColor:LPDefaultColorNormal] forState:UIControlStateNormal];
        [_showButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _showButton;
}

- (UIButton *)submitButton
{
    if (!_submitButton)
    {
        _submitButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _submitButton.frame = CGRectMake(20, 225, [[UIScreen mainScreen] bounds].size.width-40, 44);
        _submitButton.layer.masksToBounds = YES;
        _submitButton.layer.cornerRadius = 5;
        _submitButton.titleLabel.font = [UIFont systemFontOfSize:17.0f];
        _submitButton.tag = 2;
        [_submitButton setTitle:@"确认修改" forState:UIControlStateNormal];
        [_submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_submitButton setBackgroundImage:[[LPHelper shareLPHelper] imageWithColor:LPDefaultColorNormal] forState:UIControlStateNormal];
        [_submitButton setBackgroundImage:[[LPHelper shareLPHelper] imageWithColor:LPDefaultColorDisable] forState:UIControlStateDisabled];
        [_submitButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _submitButton;
}

#pragma mark - Dealloc
- (void)dealloc
{
    LPLog(@"ChangePassword dealloc");
    self.firstTextField = nil;
    self.secondTextField = nil;
    self.firstLineView = nil;
    self.secondLineView = nil;
    self.showButton = nil;
    self.submitButton = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
