/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  SettingViewController.m
 项目：  SuperTeacher
 时间：  16/1/21
 所有：  Copyright (c) 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import "SettingViewController.h"
#import "SettingTableViewCell.h"
#import "ChangePasswordViewController.h"
#import "FeedbackViewController.h"
#import "AboutUsViewController.h"
#import "AppDelegate.h"
#import "GuideViewController.h"

@interface SettingViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataSource;

@end

@implementation SettingViewController

#pragma mark - Lifecycle
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"设置";
    self.navigationController.navigationBar.tintColor = LPDefaultColorNormal;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: LPHEX(0x666666), NSFontAttributeName: [UIFont systemFontOfSize:18.0f]};
    self.navigationController.navigationBar.translucent = YES;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(leftBarButtonItemClicked:)];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationController.navigationBar setBackgroundImage:[[LPHelper shareLPHelper] imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    
    [self.view addSubview:self.tableView];
}


#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.dataSource count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSource[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"SettingCell";
    SettingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == NULL)
    {
        cell = [[SettingTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    cell.titleLabel.text = self.dataSource[indexPath.section][indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 48.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 12.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 1)
    {
        return 108.0f;
    }
    return 12.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0)
    {
        NSLog(@"修改密码");
        ChangePasswordViewController *changePasswordVC = [[ChangePasswordViewController alloc] init];
        [self.navigationController pushViewController:changePasswordVC animated:YES];
    }
    else
    {
        switch (indexPath.row)
        {
            case 0:
            {
                NSLog(@"帮助与反馈");
                FeedbackViewController *feedbackVC = [[FeedbackViewController alloc] init];
                [self.navigationController pushViewController:feedbackVC animated:YES];
            }
                break;
            case 1:
            {
                NSLog(@"关于我们");
                AboutUsViewController *aboutUsVC = [[AboutUsViewController alloc] init];
                [self.navigationController pushViewController:aboutUsVC animated:YES];
            }
                break;
                
            default:
                break;
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == 1)
    {
        UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        button.frame = CGRectMake(20, 32, [[UIScreen mainScreen] bounds].size.width-40, 44);
        button.layer.masksToBounds = YES;
        button.layer.cornerRadius = 5;
        button.layer.borderWidth = 1;
        button.layer.borderColor = LPSeparatorColor.CGColor;
        button.titleLabel.font = [UIFont systemFontOfSize:17.0f];
        [button setTitle:@"退出登录" forState:UIControlStateNormal];
        [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:button];
        
        return view;
    }
    return nil;
}

#pragma mark - CustomDelegate
#pragma mark - Event and Responce
- (void)leftBarButtonItemClicked:(UIBarButtonItem *)barButtonItem
{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

- (void)buttonClicked:(UIButton *)button
{
    [LPActionSheetView showActionSheetWithTitle:@"" cancelButtonTitle:@"取消" destructiveButtonTitle:@"退出登录" otherButtonTitles:nil handler:^(LPActionSheetView *actionSheetView, NSInteger index) {
        switch (index) {
            case -1:
            {
                LPLog(@"退出登录");
                NSMutableDictionary *params = [NSMutableDictionary dictionary];
                [params setObject:[LoginHelper getUserID] forKey:@"uid"];

                [Api requestWithMethod:@"POST" path:API_LOGOUT_URL params:params success:^(id responseObject) {
                    LPLog(@"%@", responseObject);
                    if ([responseObject[@"code"] intValue] == 0)
                    {
                        [LoginHelper saveUserName:@""];
                        [LoginHelper saveUserID:@""];
                        [LoginHelper saveRealName:@""];
                        [LoginHelper savePhotoUrl:@""];
                        [LoginHelper saveClassStatus:@""];
                        
                        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                        appDelegate.window.rootViewController = [[GuideViewController alloc] init];
                        [appDelegate.window makeKeyAndVisible];
                    }
                    else
                    {
                        [ProgressHelper showTextHUDAddedTo:self.view text:responseObject[@"msg"] animated:YES];
                    }
                } failed:^(NSError *error) {
                    [ProgressHelper showTextHUDAddedTo:self.view text:[error localizedDescription] animated:YES];
                }];
            }
                break;
                
            default:
            {
                LPLog(@"取消");
            }
                break;
        }
    }];
}

#pragma mark - Notification Methods
#pragma mark - Private Methods
#pragma mark - Setters and Getters
- (UITableView *)tableView
{
    if (!_tableView)
    {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-64) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.dataSource = self;
        _tableView.delegate = self;
    }
    return _tableView;
}

- (NSMutableArray *)dataSource
{
    if (!_dataSource)
    {
        _dataSource = [NSMutableArray arrayWithArray:@[@[@"修改密码"], @[@"帮助与反馈", @"关于我们"]]];
    }
    return _dataSource;
}

#pragma mark - Dealloc
- (void)dealloc
{
    LPLog(@"Setting dealloc");
    self.tableView = nil;
    self.dataSource = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
