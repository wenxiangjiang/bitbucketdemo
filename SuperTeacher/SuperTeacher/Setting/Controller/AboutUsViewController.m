/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  AboutUsViewController.m
 项目：  SuperTeacher
 时间：  16/1/21
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：  
 */

#import "AboutUsViewController.h"

@interface AboutUsViewController ()

@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *companyLabel;
@property (nonatomic, strong) UILabel *versionLabel;

@end

@implementation AboutUsViewController

#pragma mark - Lifecycle
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"关于我们";
    
    [self.view addSubview:self.iconImageView];
    [self.view addSubview:self.companyLabel];
    [self.view addSubview:self.versionLabel];
}

#pragma mark - Setters and Getters
- (UIImageView *)iconImageView
{
    if (!_iconImageView)
    {
        _iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width-150)/2, 200, 150, 150)];
        _iconImageView.image = [UIImage imageNamed:@"AboutUs_logo"];
    }
    return _iconImageView;
}

- (UILabel *)companyLabel
{
    if (!_companyLabel)
    {
        _companyLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 370, [[UIScreen mainScreen] bounds].size.width, 20)];
        _companyLabel.text = @"西安出右网络科技有限公司";
        _companyLabel.textColor = LPHEX(0xcacaca);
        _companyLabel.font = [UIFont systemFontOfSize:15];
        _companyLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _companyLabel;
}

- (UILabel *)versionLabel
{
    if (!_versionLabel)
    {
        _versionLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 400, [[UIScreen mainScreen] bounds].size.width, 20)];
        _versionLabel.text = [NSString stringWithFormat:@"Version %@", [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"]];
        _versionLabel.textColor = LPHEX(0xcacaca);
        _versionLabel.font = [UIFont systemFontOfSize:12];
        _versionLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _versionLabel;
}

#pragma mark - Dealloc
- (void)dealloc
{
    LPLog(@"AboutUs dealloc");
    self.iconImageView = nil;
    self.companyLabel = nil;
    self.versionLabel = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
