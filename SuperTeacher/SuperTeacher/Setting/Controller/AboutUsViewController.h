/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  AboutUsViewController.h
 项目：  SuperTeacher
 时间：  16/1/21
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import <UIKit/UIKit.h>

@interface AboutUsViewController : UIViewController

@end
