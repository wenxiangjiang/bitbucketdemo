//
//  main.m
//  SuperTeacher
//
//  Created by LiuPeng on 16/1/14.
//  Copyright (c) 2016年 LiuPeng. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
