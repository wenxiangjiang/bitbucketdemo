/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  LearningPortfolioViewController.m
 项目：  SuperTeacher
 时间：  16/1/21
 所有：  Copyright (c) 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import "LearningPortfolioViewController.h"
#import "LearningPortfolioTableViewCell.h"
#import "LearningPortfolioModel.h"

@interface LearningPortfolioViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataSource;

@end

@implementation LearningPortfolioViewController

#pragma mark - Lifecycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"学习档案";
    self.navigationController.navigationBar.tintColor = LPDefaultColorNormal;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: LPHEX(0x666666), NSFontAttributeName: [UIFont systemFontOfSize:18.0f]};
    self.navigationController.navigationBar.translucent = YES;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(leftBarButtonItemClicked:)];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationController.navigationBar setBackgroundImage:[[LPHelper shareLPHelper] imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    
    [self.view addSubview:self.tableView];
    
    [self prepareHeaderAndFooter];
    [self.tableView.mj_header beginRefreshing];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"LearningPortfolioCell";
    LearningPortfolioTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == NULL)
    {
        cell = [[LearningPortfolioTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
    }
    
    LearningPortfolioModel *model = self.dataSource[indexPath.row];
    
    cell.faceImageView.image = [UIImage imageNamed:model.faceString];
    cell.nameLabel.text = model.nameString;
    cell.timeLabel.text = model.timeString;
    cell.titleLabel.text = model.titleString;
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - CustomDelegate
#pragma mark - Event and Responce
- (void)leftBarButtonItemClicked:(UIBarButtonItem *)barButtonItem
{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

#pragma mark - Notification Methods
#pragma mark - Private Methods
- (void)prepareHeaderAndFooter
{
    __weak typeof(self) weakSelf = self;
    self.tableView.mj_header = [LPGifHeader headerWithRefreshingBlock:^{
        // 模拟延迟加载数据，因此2秒后才调用（真实开发中，可以移除这段gcd代码）
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [weakSelf loadDataSource:YES];
        });
    }];
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 模拟延迟加载数据，因此2秒后才调用（真实开发中，可以移除这段gcd代码）
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [weakSelf loadDataSource:NO];
        });
    }];
    
    self.tableView.mj_header.automaticallyChangeAlpha = YES;
}

- (void)loadDataSource:(BOOL)new
{
    if (new)
    {
        [self.dataSource removeAllObjects];
    }
    else
    {
        
    }
    
    for (int i = 0; i < 10; i++)
    {
        NSDictionary *dic = @{@"image": @"face", @"name": @"Emy", @"time": @"2015-10-15 23:15", @"title": @"口语天天练三个月套餐"};
        LearningPortfolioModel *model = [LearningPortfolioModel shareLearningPortfolioModelWithDictionary:dic];
        [self.dataSource addObject:model];
    }
    
    [self.tableView reloadData];
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

#pragma mark - Setters and Getters
- (UITableView *)tableView
{
    if (!_tableView)
    {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-64) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.dataSource = self;
        _tableView.delegate = self;
    }
    return _tableView;
}

- (NSMutableArray *)dataSource
{
    if (!_dataSource)
    {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

#pragma mark - Dealloc
- (void)dealloc
{
    LPLog(@"LearningPortfolio dealloc");
    self.tableView = nil;
    self.dataSource = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
