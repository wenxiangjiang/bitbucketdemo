//
//  LearningPortfolioModel.m
//  SuperTeacher
//
//  Created by LiuPeng on 16/2/27.
//  Copyright © 2016年 LiuPeng. All rights reserved.
//

#import "LearningPortfolioModel.h"

@implementation LearningPortfolioModel

+ (LearningPortfolioModel *)shareLearningPortfolioModelWithDictionary:(NSDictionary *)dic
{
    LearningPortfolioModel *model = [[LearningPortfolioModel alloc] init];
    model.faceString = dic[@"image"];
    model.nameString = dic[@"name"];
    model.timeString = dic[@"time"];
    model.titleString = dic[@"title"];
    return model;
}

- (void)dealloc
{
    self.faceString = nil;
    self.nameString = nil;
    self.timeString = nil;
    self.titleString = nil;
}

@end
