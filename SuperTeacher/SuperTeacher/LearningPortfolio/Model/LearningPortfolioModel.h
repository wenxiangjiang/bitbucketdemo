//
//  LearningPortfolioModel.h
//  SuperTeacher
//
//  Created by LiuPeng on 16/2/27.
//  Copyright © 2016年 LiuPeng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LearningPortfolioModel : NSObject

@property (nonatomic, copy) NSString *faceString;
@property (nonatomic, copy) NSString *nameString;
@property (nonatomic, copy) NSString *timeString;
@property (nonatomic, copy) NSString *titleString;

+ (LearningPortfolioModel *)shareLearningPortfolioModelWithDictionary:(NSDictionary *)dic;

@end
