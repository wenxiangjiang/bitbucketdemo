//
//  CViewController.m
//  SuperTeacher
//
//  Created by LiuPeng on 16/1/14.
//  Copyright (c) 2016年 LiuPeng. All rights reserved.
//

#import "CViewController.h"
#import "AppDelegate.h"
#import "IIViewDeckController.h"

@interface CViewController ()

@end

@implementation CViewController

#pragma mark - Lifecycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"超级外教";
    
    self.navigationController.navigationBar.tintColor = LPDefaultColorNormal;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: LPHEX(0x666666), NSFontAttributeName: [UIFont systemFontOfSize:18.0f]};
    self.navigationController.navigationBar.translucent = YES;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(leftBarButtonItemClicked:)];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationController.navigationBar setBackgroundImage:[[LPHelper shareLPHelper] imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
}

#pragma mark - UITableViewDataSource
#pragma mark - UITableViewDelegate
#pragma mark - CustomDelegate
#pragma mark - Event and Responce
- (void)leftBarButtonItemClicked:(UIBarButtonItem *)barButtonItem
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.deckController openLeftViewAnimated:YES];
}
#pragma mark - Notification Methods
#pragma mark - Private Methods
#pragma mark - Setters and Getters
#pragma mark - Dealloc

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
