/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  PurchaseCourseViewController.m
 项目：  SuperTeacher
 时间：  16/1/16
 所有：  Copyright (c) 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import "PurchaseCourseViewController.h"
#import "PurchaseCourseDetailViewController.h"

@interface PurchaseCourseViewController () <UITextFieldDelegate>

@property (nonatomic, strong) UILabel *promptLabel;
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UIButton *submitButton;

@end

@implementation PurchaseCourseViewController

#pragma mark - Lifecycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"购买课程";
    self.navigationController.navigationBar.tintColor = LPDefaultColorNormal;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: LPHEX(0x666666), NSFontAttributeName: [UIFont systemFontOfSize:18.0f]};
    self.navigationController.navigationBar.translucent = YES;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(leftBarButtonItemClicked:)];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationController.navigationBar setBackgroundImage:[[LPHelper shareLPHelper] imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    
    [self.view addSubview:self.promptLabel];
    [self.view addSubview:self.textField];
    [self.view addSubview:self.lineView];
    [self.view addSubview:self.submitButton];
}

#pragma mark - UITableViewDataSource
#pragma mark - UITableViewDelegate
#pragma mark - UITextFieldDelegate
#pragma mark - Event and Responce
- (void)leftBarButtonItemClicked:(UIBarButtonItem *)barButtonItem
{
    [self.textField resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

- (void)textFieldValueChanged:(UITextField *)textField
{
    NSLog(@"----%@----", textField.text);
    if (textField.text.length > 0)
    {
        self.submitButton.enabled = YES;
    }
    else
    {
        self.submitButton.enabled = NO;
    }
    
    // 用于输入卡号,根据卡号位数设置UITextField的特殊字间距为10
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:textField.text];
    if (textField.text.length <= 4)
    {
        
    }
    else if (textField.text.length > 4 && textField.text.length <= 8)
    {
        [attString addAttribute:NSKernAttributeName value:@10 range:NSMakeRange(3, 1)];
    }
    else if (textField.text.length > 8 && textField.text.length <= 12)
    {
        [attString addAttribute:NSKernAttributeName value:@10 range:NSMakeRange(3, 1)];
        [attString addAttribute:NSKernAttributeName value:@10 range:NSMakeRange(7, 1)];
    }
    else if (textField.text.length > 12 && textField.text.length <= 16)
    {
        [attString addAttribute:NSKernAttributeName value:@10 range:NSMakeRange(3, 1)];
        [attString addAttribute:NSKernAttributeName value:@10 range:NSMakeRange(7, 1)];
        [attString addAttribute:NSKernAttributeName value:@10 range:NSMakeRange(11, 1)];
    }
    else
    {
        [attString addAttribute:NSKernAttributeName value:@10 range:NSMakeRange(3, 1)];
        [attString addAttribute:NSKernAttributeName value:@10 range:NSMakeRange(7, 1)];
        [attString addAttribute:NSKernAttributeName value:@10 range:NSMakeRange(11, 1)];
        [attString deleteCharactersInRange:NSMakeRange(textField.text.length-1, textField.text.length-16)];
    }
    textField.attributedText = attString;
}

- (void)buttonClicked:(UIButton *)sender
{
    [self.textField resignFirstResponder];
    PurchaseCourseDetailViewController *purchaseCourseDetailVC = [[PurchaseCourseDetailViewController alloc] init];
    [self.navigationController pushViewController:purchaseCourseDetailVC animated:YES];
}

#pragma mark - Notification Methods
#pragma mark - Private Methods
#pragma mark - Setters and Getters
- (UILabel *)promptLabel
{
    if (!_promptLabel)
    {
        _promptLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 84, [[UIScreen mainScreen] bounds].size.width-40, 20)];
        _promptLabel.textColor = LPHEX(0x9e9e9e);
        _promptLabel.font = [UIFont systemFontOfSize:13.0f];
        _promptLabel.text = @"请输入课程卡卡号";
    }
    return _promptLabel;
}

- (UITextField *)textField
{
    if (!_textField)
    {
        _textField = [[UITextField alloc] initWithFrame:CGRectMake(20, 120, [[UIScreen mainScreen] bounds].size.width-40, 30)];
        _textField.font = [UIFont systemFontOfSize:15.0f];
        _textField.tintColor = LPDefaultColorNormal;
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textField.autocorrectionType = UITextAutocorrectionTypeNo;
        _textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _textField.keyboardType = UIKeyboardTypeASCIICapable;
        [_textField becomeFirstResponder];
        [_textField addTarget:self action:@selector(textFieldValueChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    return _textField;
}

- (UIView *)lineView
{
    if (!_lineView)
    {
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(20, 150, [[UIScreen mainScreen] bounds].size.width-40, LPSeparatorHeight)];
        _lineView.backgroundColor = LPSeparatorColor;
    }
    return _lineView;
}

- (UIButton *)submitButton
{
    if (!_submitButton)
    {
        _submitButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _submitButton.frame = CGRectMake(20, 180, [[UIScreen mainScreen] bounds].size.width-40, 44);
        _submitButton.layer.masksToBounds = YES;
        _submitButton.layer.cornerRadius = 5;
        _submitButton.titleLabel.font = [UIFont systemFontOfSize:17.0f];
        _submitButton.enabled = NO;
        [_submitButton setTitle:@"确定" forState:UIControlStateNormal];
        [_submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_submitButton setBackgroundImage:[[LPHelper shareLPHelper] imageWithColor:LPDefaultColorNormal] forState:UIControlStateNormal];
        [_submitButton setBackgroundImage:[[LPHelper shareLPHelper] imageWithColor:LPDefaultColorDisable] forState:UIControlStateDisabled];
        [_submitButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _submitButton;
}

#pragma mark - Dealloc
- (void)dealloc
{
    LPLog(@"PurchaseCourse dealloc");
    self.promptLabel = nil;
    self.textField = nil;
    self.lineView = nil;
    self.submitButton = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
