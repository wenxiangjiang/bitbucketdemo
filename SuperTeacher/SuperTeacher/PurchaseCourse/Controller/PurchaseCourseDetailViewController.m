/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  PurchaseCourseDetailViewController.m
 项目：  SuperTeacher
 时间：  16/1/16
 所有：  Copyright (c) 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import "PurchaseCourseDetailViewController.h"
#import "PurchaseCourseDetailTableViewCell.h"

@interface PurchaseCourseDetailViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataSource;

@end

@implementation PurchaseCourseDetailViewController

#pragma mark - Lifecycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"课程卡详情";
    
    self.dataSource = [NSMutableArray arrayWithArray:@[@{@"title": @"课程卡号", @"detail": @"3509 1024 6688 5478"},
                                                       @{@"title": @"卡标题", @"detail": @"口语天天练三个月课程套餐"},
                                                       @{@"title": @"套餐内容", @"detail": @"1对1真人外教口语天天练，3个月套餐，价值150元，启东中学专享，3个月套餐，价值150元，启东中学专享"},
                                                       @{@"title": @"有效时长", @"detail": @"3个月"}]];
    
    [self.view addSubview:self.tableView];
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"PurchaseCourseDetailTableViewCell";
    PurchaseCourseDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == NULL)
    {
        cell = [[PurchaseCourseDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    
    cell.titleLabel.text = self.dataSource[indexPath.row][@"title"];
    cell.detailLabel.text = self.dataSource[indexPath.row][@"detail"];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *str = self.dataSource[indexPath.row][@"detail"];
    CGSize size = [str boundingRectWithSize:CGSizeMake([[UIScreen mainScreen] bounds].size.width-130, 300) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15]} context:nil].size;
    return MAX(size.height + 20, 50);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 108.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(20, 32, [[UIScreen mainScreen] bounds].size.width-40, 44);
    button.layer.masksToBounds = YES;
    button.layer.cornerRadius = 5;
    button.titleLabel.font = [UIFont systemFontOfSize:17.0f];
    [button setTitle:@"使用此卡" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setBackgroundImage:[[LPHelper shareLPHelper] imageWithColor:LPDefaultColorNormal] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:button];
    
    return view;
}

#pragma mark - CustomDelegate
#pragma mark - Event and Responce
- (void)buttonClicked:(UIButton *)sender
{
    NSLog(@"使用此卡");
    
    [ProgressHelper showImageHUDAddedTo:[[[UIApplication sharedApplication] delegate] window] image:[UIImage imageNamed:@"Toast_Success"] text:@"购买成功" animated:YES];
    [self performSelector:@selector(dismissViewController) withObject:nil afterDelay:1.5];
}

- (void)dismissViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Notification Methods
#pragma mark - Private Methods
#pragma mark - Setters and Getters
- (UITableView *)tableView
{
    if (!_tableView)
    {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-64) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.dataSource = self;
        _tableView.delegate = self;
    }
    return _tableView;
}

- (NSMutableArray *)dataSource
{
    if (!_dataSource)
    {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

#pragma mark - Dealloc
- (void)dealloc
{
    LPLog(@"PurchaseCourseDetail dealloc");
    self.tableView = nil;
    self.dataSource = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
