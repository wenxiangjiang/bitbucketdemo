/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  PurchaseCourseDetailTableViewCell.m
 项目：  SuperTeacher
 时间：  16/1/16
 所有：  Copyright (c) 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import "PurchaseCourseDetailTableViewCell.h"

@implementation PurchaseCourseDetailTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.textColor = LPHEX(0x9e9e9e);
        self.titleLabel.font = [UIFont systemFontOfSize:15.0f];
        [self.contentView addSubview:self.titleLabel];
        
        self.detailLabel = [[UILabel alloc] init];
        self.detailLabel.textColor = LPHEX(0x404040);
        self.detailLabel.font = [UIFont systemFontOfSize:15.0f];
        self.detailLabel.numberOfLines = 0;
        [self.contentView addSubview:self.detailLabel];
        
        self.lineView = [[UIView alloc] init];
        self.lineView.backgroundColor = LPSeparatorColor;
        [self.contentView addSubview:self.lineView];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.titleLabel.frame = CGRectMake(20, 10, 60, 30);
    self.detailLabel.frame = CGRectMake(110, 10, self.frame.size.width-130, self.frame.size.height-20);
    self.lineView.frame = CGRectMake(20, self.frame.size.height-LPSeparatorHeight, self.frame.size.width-20, LPSeparatorHeight);
}

- (void)dealloc
{
    self.titleLabel = nil;
    self.detailLabel = nil;
    self.lineView = nil;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
