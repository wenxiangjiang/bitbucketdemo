//
//  PurchaseCourseDetailModel.m
//  SuperTeacher
//
//  Created by LiuPeng on 16/2/27.
//  Copyright © 2016年 LiuPeng. All rights reserved.
//

#import "PurchaseCourseDetailModel.h"

@implementation PurchaseCourseDetailModel

+ (PurchaseCourseDetailModel *)sharePurchaseCourseDetailModelWithDictionary:(NSDictionary *)dic
{
    PurchaseCourseDetailModel *model = [[PurchaseCourseDetailModel alloc] init];
    model.titleString = dic[@"title"];
    model.detailString = dic[@"detail"];
    return model;
}

@end
