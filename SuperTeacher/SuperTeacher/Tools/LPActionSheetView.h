/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  LPActionSheetView.h
 项目：  SuperTeacher
 时间：  16/1/22
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import <UIKit/UIKit.h>

/**
 * 取消index = 0
 * 删除index = -1
 * 其他index从1开始递增
 */

@class LPActionSheetView;

typedef void(^LPActionSheetBlock)(LPActionSheetView *actionSheetView, NSInteger index);

@interface LPActionSheetView : UIView

- (instancetype)initWithTitle:(NSString *)title cancelButtonTitle:(NSString *)cancelButtonTitle destructiveButtonTitle:(NSString *)destructiveButtonTitle otherButtonTitles:(NSArray *)otherButtonTitles handler:(LPActionSheetBlock)block;

+ (void)showActionSheetWithTitle:(NSString *)title cancelButtonTitle:(NSString *)cancelButtonTitle destructiveButtonTitle:(NSString *)destructiveButtonTitle otherButtonTitles:(NSArray *)otherButtonTitles handler:(LPActionSheetBlock)block;

- (void)show;
- (void)dismiss;

@end
