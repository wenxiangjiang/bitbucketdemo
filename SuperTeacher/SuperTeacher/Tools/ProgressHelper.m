/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  ProgressHelper.m
 项目：  MBProgressViewDemo
 时间：  16/1/25
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import "ProgressHelper.h"
#import "MBProgressHUD.h"

@implementation ProgressHelper

+ (MBProgressHUD *)showImageHUDAddedTo:(UIView *)view image:(UIImage *)image text:(NSString *)text animated:(BOOL)animated
{
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:view animated:animated];
    progressHUD.mode = MBProgressHUDModeCustomView;
    progressHUD.animationType = MBProgressHUDAnimationZoomIn;
    progressHUD.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
    progressHUD.color = [UIColor whiteColor];
    progressHUD.square = YES;
    progressHUD.customView = [[UIImageView alloc] initWithImage:image];
    progressHUD.labelText = text;
    progressHUD.labelColor = [UIColor blackColor];
    [progressHUD hide:YES afterDelay:1];
    
    return progressHUD;
}

+ (MBProgressHUD *)showWaitHUDAddedTo:(UIView *)view text:(NSString *)text animated:(BOOL)animated
{
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:view animated:animated];
    progressHUD.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
    progressHUD.labelText = text;
    
    return progressHUD;
}

+ (MBProgressHUD *)showTextHUDAddedTo:(UIView *)view text:(NSString *)text animated:(BOOL)animated
{
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:view animated:animated];
    progressHUD.mode = MBProgressHUDModeText;
    progressHUD.margin = 10;
    progressHUD.yOffset = ([[UIScreen mainScreen] bounds].size.height-64)/4;
    progressHUD.labelText = text;
    [progressHUD hide:YES afterDelay:1];
    
    return progressHUD;
}

@end
