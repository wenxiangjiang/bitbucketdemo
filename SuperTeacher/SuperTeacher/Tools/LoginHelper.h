/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  LoginHelper.h
 项目：  SuperTeacher
 时间：  16/1/22
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：  用于保存与获取登录数据
 */

#import <Foundation/Foundation.h>

@interface LoginHelper : NSObject

#pragma mark - 保存登陆信息
/**
 * 保存上课状态
 */
+ (void)saveClassStatus:(NSString *)classStatus;

/**
 * 保存电话号码
 */
+ (void)savePhoneNumber:(NSString *)phoneNumber;

/**
 * 保存用户名
 */
+ (void)saveUserName:(NSString *)userName;

/**
 * 保存UID
 */
+ (void)saveUserID:(NSString *)userID;

/**
 * 保存头像
 */
+ (void)savePhotoUrl:(NSString *)photoUrl;

/**
 * 保存家长电话号码
 */
+ (void)saveFamilyPhoneNumber:(NSString *)familyPhoneNumber;

/**
 * 保存真实姓名
 */
+ (void)saveRealName:(NSString *)realName;

/**
 * 保存性别
 */
+ (void)saveSex:(BOOL)sex;

/**
 * 保存生日
 */
+ (void)saveBirthday:(NSString *)birthday;

/**
 * 保存地区
 */
+ (void)saveArea:(NSString *)area;

/**
 * 保存学校
 */
+ (void)saveSchool:(NSString *)school;

/**
 * 保存年级
 */
+ (void)saveGrade:(NSString *)grade;

#pragma mark - 获取登陆信息
/**
 * 获取上课状态
 */
+ (NSString *)getClassStatus;

/**
 * 获取电话号码
 */
+ (NSString *)getPhoneNumber;

/**
 * 获取用户名
 */
+ (NSString *)getUserName;

/**
 * 获取UID
 */
+ (NSString *)getUserID;

/**
 * 获取头像
 */
+ (NSString *)getPhotoUrl;

/**
 * 获取家长电话号码
 */
+ (NSString *)getFamilyPhoneNumber;

/**
 * 获取真实姓名
 */
+ (NSString *)getRealName;

/**
 * 获取性别
 */
+ (BOOL)getSex;

/**
 * 获取生日
 */
+ (NSString *)getBirthday;

/**
 * 获取地区
 */
+ (NSString *)getArea;

/**
 * 获取学校
 */
+ (NSString *)getSchool;

/**
 * 获取年级
 */
+ (NSString *)getGrade;

@end
