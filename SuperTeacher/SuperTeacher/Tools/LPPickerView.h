/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  LPPickerView.h
 项目：  SuperTeacher
 时间：  16/1/22
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import <UIKit/UIKit.h>

/**
 * index为所选数据下标
 */

@class LPPickerView;

typedef void(^LPPickerViewBlock)(LPPickerView *pickerView, NSInteger index);

@interface LPPickerView : UIView

- (instancetype)initWithTitle:(NSString *)title dataTitles:(NSArray *)dataTitles handler:(LPPickerViewBlock)block;

+ (void)showPickerViewWithTitle:(NSString *)title dataTitles:(NSArray *)dataTitles handler:(LPPickerViewBlock)block;

- (void)show;
- (void)dismiss;

@end

