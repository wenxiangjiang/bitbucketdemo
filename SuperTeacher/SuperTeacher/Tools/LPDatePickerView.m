/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  LPDatePickerView.m
 项目：  SuperTeacher
 时间：  16/1/22
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import "LPDatePickerView.h"

static const CGFloat kRowHeight = 50.0f;
static const CGFloat kTitleFontSize = 15.0f;
static const CGFloat kButtonTitleFontSize = 17.0f;

@interface LPDatePickerView ()
{
    UIView *_backView;
    UIView * _datePickerView;
    BOOL _isShow;
}

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) LPDatePickerViewBlock selectRowBlock;
@property (nonatomic, strong) UIDatePicker *datePicker;

@end

@implementation LPDatePickerView

- (instancetype)initWithTitle:(NSString *)title handler:(LPDatePickerViewBlock)block
{
    self = [super init];
    if (self)
    {
        self.title = title;
        self.selectRowBlock = block;
        
        _isShow = NO;
        
        self.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
        
        _backView = [[UIView alloc] initWithFrame:self.frame];
        _backView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4f];
        _backView.alpha = 0;
        [self addSubview:_backView];
        
        _datePickerView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.frame), CGRectGetWidth(self.frame), 310)];
        _datePickerView.backgroundColor = [UIColor colorWithRed:230.0f/255.0f green:230.0f/255.0f blue:230.0f/255.0f alpha:1.0f];
        [self addSubview:_datePickerView];
        
        UIImage *normalImage = [self imageWithColor:LPDefaultColorNormal];
        UIImage *highlightedImage = [self imageWithColor:LPDefaultColorDisable];
        
        if (self.title && self.title.length > 0)
        {
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), 44)];
            titleLabel.backgroundColor = LPHEX(0xf7f7f7);
            titleLabel.textColor = LPHEX(0x404040);
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.font = [UIFont systemFontOfSize:kTitleFontSize];
            titleLabel.text = self.title;
            [_datePickerView addSubview:titleLabel];
        }
        
        UIButton *cancleButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        cancleButton.frame = CGRectMake(0, 0, 44, 44);
        cancleButton.tag = 1;
        cancleButton.titleLabel.font = [UIFont systemFontOfSize:kButtonTitleFontSize];
        [cancleButton setTitle:@"取消" forState:UIControlStateNormal];
        [cancleButton setTitleColor:LPDefaultColorNormal forState:UIControlStateNormal];
        [cancleButton setBackgroundColor:LPHEX(0xf7f7f7)];
        [cancleButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_datePickerView addSubview:cancleButton];
        
        self.datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 44, CGRectGetWidth(self.frame), 216)];
        self.datePicker.datePickerMode = UIDatePickerModeDate;
        self.datePicker.locale = [NSLocale localeWithLocaleIdentifier:@"zh"];
        self.datePicker.maximumDate = [NSDate date];
        [_datePickerView addSubview:self.datePicker];
        
        UIButton *submitButton = [UIButton buttonWithType:UIButtonTypeCustom];
        submitButton.frame = CGRectMake(0, 260, CGRectGetWidth(self.frame), 50);
        submitButton.tag = 2;
        submitButton.titleLabel.font = [UIFont systemFontOfSize:kButtonTitleFontSize];
        [submitButton setTitle:@"确定" forState:UIControlStateNormal];
        [submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [submitButton setBackgroundImage:normalImage forState:UIControlStateNormal];
        [submitButton setBackgroundImage:highlightedImage forState:UIControlStateHighlighted];
        [submitButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_datePickerView addSubview:submitButton];
        
    }
    
    return self;
}

+ (void)showDatePickerViewWithTitle:(NSString *)title handler:(LPDatePickerViewBlock)block
{
    LPDatePickerView *datePickerView = [[LPDatePickerView alloc] initWithTitle:title handler:block];
    [datePickerView show];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:_backView];
    if (!CGRectContainsPoint(_datePickerView.frame, point))
    {
        [self dismiss];
    }
}

- (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (void)buttonClicked:(UIButton *)button
{
    switch (button.tag)
    {
        case 1:
            break;
        case 2:
        {
            if (self.selectRowBlock)
            {
                self.selectRowBlock(self, self.datePicker.date);
            }
        }
            
        default:
            break;
    }
    [self dismiss];
}

- (void)show
{
    if(_isShow) return;
    
    _isShow = YES;
    
    [UIView animateWithDuration:0.35f delay:0 usingSpringWithDamping:0.9f initialSpringVelocity:0.7f options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionLayoutSubviews animations:^{
        [[[[UIApplication sharedApplication] delegate] window] addSubview:self];
        _backView.alpha = 1.0f;
        
        _datePickerView.frame = CGRectMake(0, CGRectGetHeight(self.frame)-310, CGRectGetWidth(self.frame), 310);
    } completion:NULL];
}

- (void)dismiss
{
    _isShow = NO;
    
    [UIView animateWithDuration:0.35f delay:0 usingSpringWithDamping:0.9f initialSpringVelocity:0.7f options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionLayoutSubviews animations:^{
        _backView.alpha = 0;
        
        _datePickerView.frame = CGRectMake(0, CGRectGetHeight(self.frame), CGRectGetWidth(self.frame), 310);
        
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)dealloc
{
    NSLog(@"PickerView dealloc");
    self.title = nil;
    self.selectRowBlock = nil;
    self.datePicker = nil;
    
    _backView = nil;
    _datePickerView = nil;
}

@end
