/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  LPPickerView.m
 项目：  SuperTeacher
 时间：  16/1/22
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import "LPPickerView.h"

static const CGFloat kRowHeight = 50.0f;
static const CGFloat kTitleFontSize = 15.0f;
static const CGFloat kButtonTitleFontSize = 17.0f;

@interface LPPickerView () <UIPickerViewDataSource, UIPickerViewDelegate>
{
    UIView *_backView;
    UIView * _pickerView;
    NSInteger _dataIndex;
    BOOL _isShow;
}

@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) NSArray *dataTitles;
@property (nonatomic, copy) LPPickerViewBlock selectRowBlock;

@end

@implementation LPPickerView

- (instancetype)initWithTitle:(NSString *)title dataTitles:(NSArray *)dataTitles handler:(LPPickerViewBlock)block
{
    self = [super init];
    if (self)
    {
        self.title = title;
        self.dataTitles = dataTitles;
        self.selectRowBlock = block;
        
        _dataIndex = 0;
        _isShow = NO;
        
        self.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
        
        _backView = [[UIView alloc] initWithFrame:self.frame];
        _backView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4f];
        _backView.alpha = 0;
        [self addSubview:_backView];
        
        _pickerView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.frame), CGRectGetWidth(self.frame), 310)];
        _pickerView.backgroundColor = [UIColor colorWithRed:230.0f/255.0f green:230.0f/255.0f blue:230.0f/255.0f alpha:1.0f];
        [self addSubview:_pickerView];
        
        UIImage *normalImage = [self imageWithColor:LPDefaultColorNormal];
        UIImage *highlightedImage = [self imageWithColor:LPDefaultColorDisable];
        
        if (self.title && self.title.length > 0)
        {
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), 44)];
            titleLabel.backgroundColor = LPHEX(0xcacaca);
            titleLabel.textColor = LPHEX(0x404040);
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.font = [UIFont systemFontOfSize:kTitleFontSize];
            titleLabel.text = self.title;
            [_pickerView addSubview:titleLabel];
        }
        
        UIButton *cancleButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        cancleButton.frame = CGRectMake(0, 0, 44, 44);
        cancleButton.tag = 1;
        cancleButton.titleLabel.font = [UIFont systemFontOfSize:kButtonTitleFontSize];
        [cancleButton setTitle:@"取消" forState:UIControlStateNormal];
        [cancleButton setTitleColor:LPDefaultColorNormal forState:UIControlStateNormal];
        [cancleButton setBackgroundColor:LPHEX(0xcacaca)];
        [cancleButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_pickerView addSubview:cancleButton];
        
        if ([self.dataTitles count] > 0)
        {
            UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, CGRectGetWidth(self.frame), 216)];
            pickerView.backgroundColor = [UIColor whiteColor];
            pickerView.dataSource = self;
            pickerView.delegate = self;
            [_pickerView addSubview:pickerView];
        }
        
        UIButton *submitButton = [UIButton buttonWithType:UIButtonTypeCustom];
        submitButton.frame = CGRectMake(0, 260, CGRectGetWidth(self.frame), 50);
        submitButton.tag = 2;
        submitButton.titleLabel.font = [UIFont systemFontOfSize:kButtonTitleFontSize];
        [submitButton setTitle:@"确定" forState:UIControlStateNormal];
        [submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [submitButton setBackgroundImage:normalImage forState:UIControlStateNormal];
        [submitButton setBackgroundImage:highlightedImage forState:UIControlStateHighlighted];
        [submitButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_pickerView addSubview:submitButton];
        
    }
    
    return self;
}

#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.dataTitles count];
}

#pragma mark - UIPickerViewDelegate
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return kRowHeight;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), kRowHeight)];
    label.text = self.dataTitles[row];
    label.textColor = LPHEX(0x404040);
    label.font = [UIFont systemFontOfSize:kTitleFontSize];
    label.textAlignment = NSTextAlignmentCenter;
    
    return label;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    _dataIndex = row;
}

+ (void)showPickerViewWithTitle:(NSString *)title dataTitles:(NSArray *)dataTitles handler:(LPPickerViewBlock)block
{
    LPPickerView *pickerView = [[LPPickerView alloc] initWithTitle:title dataTitles:dataTitles handler:block];
    [pickerView show];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:_backView];
    if (!CGRectContainsPoint(_pickerView.frame, point))
    {
        [self dismiss];
    }
}

- (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (void)buttonClicked:(UIButton *)button
{
    switch (button.tag)
    {
        case 1:
            break;
        case 2:
        {
            if (self.selectRowBlock)
            {
                self.selectRowBlock(self, _dataIndex);
            }
        }
            
        default:
            break;
    }
    [self dismiss];
}

- (void)show
{
    if(_isShow) return;
    
    _isShow = YES;
    
    [UIView animateWithDuration:0.35f delay:0 usingSpringWithDamping:0.9f initialSpringVelocity:0.7f options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionLayoutSubviews animations:^{
        [[[[UIApplication sharedApplication] delegate] window] addSubview:self];
        _backView.alpha = 1.0f;
        
        _pickerView.frame = CGRectMake(0, CGRectGetHeight(self.frame)-310, CGRectGetWidth(self.frame), 310);
    } completion:NULL];
}

- (void)dismiss
{
    _isShow = NO;
    
    [UIView animateWithDuration:0.35f delay:0 usingSpringWithDamping:0.9f initialSpringVelocity:0.7f options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionLayoutSubviews animations:^{
        _backView.alpha = 0;
        
        _pickerView.frame = CGRectMake(0, CGRectGetHeight(self.frame), CGRectGetWidth(self.frame), 310);
        
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)dealloc
{
    NSLog(@"PickerView dealloc");
    self.title = nil;
    self.dataTitles = nil;
    self.selectRowBlock = nil;
    
    _backView = nil;
    _pickerView = nil;
}

@end
