/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  LoginHelper.m
 项目：  SuperTeacher
 时间：  16/1/22
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import "LoginHelper.h"

static NSString *const kClassStatus       = @"CLASS_STATUS";
static NSString *const kPhoneNumber       = @"PHONE_NUMBER";
static NSString *const kUserName          = @"USER_NAME";
static NSString *const kUserID            = @"USER_ID";
static NSString *const kPhotoUrl          = @"PHOTO_URL";
static NSString *const kFamilyPhoneNumber = @"FAMILY_PHONE_NUMBER";
static NSString *const kRealName          = @"REAL_NAME";
static NSString *const kSex               = @"SEX";
static NSString *const kBirthday          = @"BIRTHDAY";
static NSString *const kArea              = @"AREA";
static NSString *const kSchool            = @"SCHOOL";
static NSString *const kGrade             = @"GRADE";

@implementation LoginHelper

#pragma mark - 保存登陆信息
/**
 * 保存上课状态
 */
+ (void)saveClassStatus:(NSString *)classStatus
{
    [[NSUserDefaults standardUserDefaults] setObject:classStatus forKey:kClassStatus];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/**
 * 保存电话号码
 */
+ (void)savePhoneNumber:(NSString *)phoneNumber
{
    [[NSUserDefaults standardUserDefaults] setObject:phoneNumber forKey:kPhoneNumber];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/**
 * 保存用户名
 */
+ (void)saveUserName:(NSString *)userName
{
    [[NSUserDefaults standardUserDefaults] setObject:userName forKey:kUserName];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/**
 * 保存UID
 */
+ (void)saveUserID:(NSString *)userID
{
    [[NSUserDefaults standardUserDefaults] setObject:userID forKey:kUserID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/**
 * 保存头像
 */
+ (void)savePhotoUrl:(NSString *)photoUrl
{
    [[NSUserDefaults standardUserDefaults] setObject:photoUrl forKey:kPhotoUrl];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/**
 * 保存家长电话号码
 */
+ (void)saveFamilyPhoneNumber:(NSString *)familyPhoneNumber
{
    [[NSUserDefaults standardUserDefaults] setObject:familyPhoneNumber forKey:kFamilyPhoneNumber];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/**
 * 保存真实姓名
 */
+ (void)saveRealName:(NSString *)realName
{
    [[NSUserDefaults standardUserDefaults] setObject:realName forKey:kRealName];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/**
 * 保存性别
 */
+ (void)saveSex:(BOOL)sex
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:sex] forKey:kSex];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/**
 * 保存生日
 */
+ (void)saveBirthday:(NSString *)birthday
{
    [[NSUserDefaults standardUserDefaults] setObject:birthday forKey:kBirthday];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/**
 * 保存地区
 */
+ (void)saveArea:(NSString *)area
{
    [[NSUserDefaults standardUserDefaults] setObject:area forKey:kArea];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/**
 * 保存学校
 */
+ (void)saveSchool:(NSString *)school
{
    [[NSUserDefaults standardUserDefaults] setObject:school forKey:kSchool];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/**
 * 保存年级
 */
+ (void)saveGrade:(NSString *)grade
{
    [[NSUserDefaults standardUserDefaults] setObject:grade forKey:kGrade];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - 获取登陆信息
/**
 * 获取上课状态
 */
+ (NSString *)getClassStatus
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kClassStatus];
}

/**
 * 获取电话号码
 */
+ (NSString *)getPhoneNumber
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kPhoneNumber];
}

/**
 * 获取用户名
 */
+ (NSString *)getUserName
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kUserName];
}

/**
 * 获取UID
 */
+ (NSString *)getUserID
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kUserID];
}

/**
 * 获取头像
 */
+ (NSString *)getPhotoUrl
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kPhotoUrl];
}

/**
 * 获取家长电话号码
 */
+ (NSString *)getFamilyPhoneNumber
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kFamilyPhoneNumber];
}

/**
 * 获取真实姓名
 */
+ (NSString *)getRealName
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kRealName];
}

/**
 * 获取性别
 */
+ (BOOL)getSex
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kSex];
}

/**
 * 获取生日
 */
+ (NSString *)getBirthday
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kBirthday];
}

/**
 * 获取地区
 */
+ (NSString *)getArea
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kArea];
}

/**
 * 获取学校
 */
+ (NSString *)getSchool
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kSchool];
}

/**
 * 获取年级
 */
+ (NSString *)getGrade
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kGrade];
}

@end
