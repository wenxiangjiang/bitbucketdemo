/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  LPDatePicker.m
 项目：  SuperTeacher
 时间：  16/1/25
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import "LPDatePicker.h"

@implementation LPDatePicker

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
