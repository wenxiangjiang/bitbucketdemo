/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  LPDatePickerView.h
 项目：  SuperTeacher
 时间：  16/1/22
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import <UIKit/UIKit.h>

@class LPDatePickerView;

typedef void(^LPDatePickerViewBlock)(LPDatePickerView *dataPickerView, NSDate *date);

@interface LPDatePickerView : UIView

- (instancetype)initWithTitle:(NSString *)title handler:(LPDatePickerViewBlock)block;

+ (void)showDatePickerViewWithTitle:(NSString *)title handler:(LPDatePickerViewBlock)block;

- (void)show;
- (void)dismiss;

@end