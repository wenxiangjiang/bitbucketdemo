/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  LPHelper.h
 项目：  SuperTeacher
 时间：  16/1/22
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：  用于封装一些常用工具方法
 */

#import <Foundation/Foundation.h>
#import "LPSingleton.h"

@interface LPHelper : NSObject <NSCopying, NSMutableCopying>

interfaceSingleton(LPHelper)

/**
 * 通过(UIColor *)颜色生成(UIImage *)图片
 */
- (UIImage *)imageWithColor:(UIColor *)color;

@end
