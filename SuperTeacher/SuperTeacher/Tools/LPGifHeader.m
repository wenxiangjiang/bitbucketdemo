//
//  LPGifHeader.m
//  shua
//
//  Created by LiuPeng on 16/1/18.
//  Copyright © 2016年 LiuPeng. All rights reserved.
//

#import "LPGifHeader.h"

@interface LPGifHeader ()

//@property (weak, nonatomic) UIImageView *logoImageView;

@end

@implementation LPGifHeader

#pragma mark - 重写方法
#pragma mark 在这里做一些初始化配置（比如添加子控件）
- (void)prepare
{
    [super prepare];
    
    self.backgroundColor = [UIColor clearColor];
    // 设置控件的高度
    self.mj_h = 64;

    self.lastUpdatedTimeLabel.hidden = YES;
    
//    UIImageView *logoImageView = [[UIImageView alloc] init];
//    logoImageView.image = [UIImage imageNamed:@"1024"];
//    [self addSubview:logoImageView];
//    self.logoImageView = logoImageView;
    
    // 设置普通状态的动画图片
    NSMutableArray *idleImages = [NSMutableArray array];
    for (NSUInteger i = 1; i<=67; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"dropdown_anim__000%zd", i]];
        [idleImages addObject:image];
    }
    [self setImages:idleImages forState:MJRefreshStateIdle];
    
    // 设置即将刷新状态的动画图片（一松开就会刷新的状态）
    NSMutableArray *refreshingImages = [NSMutableArray array];
    for (NSUInteger i = 1; i<=67; i ++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"dropdown_anim__000%zd", i]];
        [refreshingImages addObject:image];
    }
    [self setImages:refreshingImages duration:1.2 forState:MJRefreshStatePulling];
    
    // 设置正在刷新状态的动画图片
    [self setImages:refreshingImages duration:1.2 forState:MJRefreshStateRefreshing];
}

#pragma mark 在这里设置子控件的位置和尺寸
- (void)placeSubviews
{
    [super placeSubviews];
    
//    self.logoImageView.frame = CGRectMake(0, -100, self.mj_w, 100);
    [self.gifView setFrame:CGRectMake((self.mj_w-33)/2, 3, 33, 33)];
    [self.stateLabel setFrame:CGRectMake(0, CGRectGetMaxY(self.gifView.frame) + 10, self.mj_w, 18)];
    self.stateLabel.font = [UIFont systemFontOfSize:12];
}

@end
