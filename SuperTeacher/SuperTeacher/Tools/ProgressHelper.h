/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  ProgressHelper.h
 项目：  MBProgressViewDemo
 时间：  16/1/25
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：  用于封装一些Progress相关方法
 */

#import <Foundation/Foundation.h>
@class MBProgressHUD;

@interface ProgressHelper : NSObject

/**
 * 展示提醒框,带有自定义图片与文字,框体为正方形,1s后消失
 */
+ (MBProgressHUD *)showImageHUDAddedTo:(UIView *)view image:(UIImage *)image text:(NSString *)text animated:(BOOL)animated;

/**
 * 展示等待框,带有自定义文字
 */
+ (MBProgressHUD *)showWaitHUDAddedTo:(UIView *)view text:(NSString *)text animated:(BOOL)animated;

/**
 * 展示提醒框,带有自定义文字,1s后消失
 */
+ (MBProgressHUD *)showTextHUDAddedTo:(UIView *)view text:(NSString *)text animated:(BOOL)animated;

@end
