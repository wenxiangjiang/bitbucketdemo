/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  LPHelper.m
 项目：  SuperTeacher
 时间：  16/1/22
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import "LPHelper.h"

@implementation LPHelper

implementationSingleton(LPHelper)

/**
 * 通过(UIColor *)颜色生成(UIImage *)图片
 */
- (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
