/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  PurchaseHistoryTableViewCell.h
 项目：  SuperTeacher
 时间：  16/1/21
 所有：  Copyright (c) 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import <UIKit/UIKit.h>

@interface PurchaseHistoryTableViewCell : UITableViewCell

@property (nonatomic, strong) UIImageView *faceImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UIView *lineView;

@end
