/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  PurchaseHistoryTableViewCell.m
 项目：  SuperTeacher
 时间：  16/1/21
 所有：  Copyright (c) 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import "PurchaseHistoryTableViewCell.h"

@implementation PurchaseHistoryTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        self.faceImageView = [[UIImageView alloc] init];
        self.faceImageView.layer.masksToBounds = YES;
        self.faceImageView.layer.cornerRadius = 5.0f;
        [self.contentView addSubview:self.faceImageView];
        
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.textColor = LPHEX(0x404040);
        self.titleLabel.font = [UIFont systemFontOfSize:15.0f];
        [self.contentView addSubview:self.titleLabel];
        
        self.timeLabel = [[UILabel alloc] init];
        self.timeLabel.textColor = LPHEX(0x9e9e9e);
        self.timeLabel.font = [UIFont systemFontOfSize:12.0f];
        [self.contentView addSubview:self.timeLabel];
        
        self.priceLabel = [[UILabel alloc] init];
        self.priceLabel.textColor = LPHEX(0x404040);
        self.priceLabel.font = [UIFont systemFontOfSize:17.0f];
        [self.contentView addSubview:self.priceLabel];
        
        self.lineView = [[UIView alloc] init];
        self.lineView.backgroundColor = LPSeparatorColor;
        [self.contentView addSubview:self.lineView];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.faceImageView.frame = CGRectMake(20, 15, 40, 40);
    self.titleLabel.frame = CGRectMake(80, 15, self.frame.size.width-140, 20);
    self.timeLabel.frame = CGRectMake(80, 35, self.frame.size.width-140, 20);
    self.priceLabel.frame = CGRectMake(self.frame.size.width-60, 0, 60, 70);
    self.lineView.frame = CGRectMake(20, self.frame.size.height-LPSeparatorHeight, self.frame.size.width-20, LPSeparatorHeight);
}

- (void)dealloc
{
    self.faceImageView = nil;
    self.titleLabel = nil;
    self.timeLabel = nil;
    self.priceLabel = nil;
    self.lineView = nil;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
    self.lineView.backgroundColor = LPSeparatorColor;
}

@end
