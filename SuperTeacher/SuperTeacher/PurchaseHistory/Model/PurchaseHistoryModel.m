//
//  PurchaseHistoryModel.m
//  SuperTeacher
//
//  Created by LiuPeng on 16/2/27.
//  Copyright © 2016年 LiuPeng. All rights reserved.
//

#import "PurchaseHistoryModel.h"

@implementation PurchaseHistoryModel

+ (PurchaseHistoryModel *)sharePurchaseHistoryModelWithDictionary:(NSDictionary *)dic;
{
    PurchaseHistoryModel *model = [[PurchaseHistoryModel alloc] init];
    model.faceString = dic[@"image"];
    model.titleString = dic[@"title"];
    model.timeString = dic[@"time"];
    model.priceString = dic[@"price"];
    return model;
}

- (void)dealloc
{
    self.faceString = nil;
    self.titleString = nil;
    self.timeString = nil;
    self.priceString = nil;
}

@end
