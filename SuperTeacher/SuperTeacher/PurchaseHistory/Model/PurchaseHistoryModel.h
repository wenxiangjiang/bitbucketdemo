//
//  PurchaseHistoryModel.h
//  SuperTeacher
//
//  Created by LiuPeng on 16/2/27.
//  Copyright © 2016年 LiuPeng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PurchaseHistoryModel : NSObject

@property (nonatomic, copy) NSString *faceString;
@property (nonatomic, copy) NSString *titleString;
@property (nonatomic, copy) NSString *timeString;
@property (nonatomic, copy) NSString *priceString;

+ (PurchaseHistoryModel *)sharePurchaseHistoryModelWithDictionary:(NSDictionary *)dic;

@end
