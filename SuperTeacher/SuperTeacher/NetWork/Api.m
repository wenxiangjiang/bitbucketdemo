/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  Api.m
 项目：  SuperTeacher
 时间：  16/1/27
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import "Api.h"
#import <CommonCrypto/CommonDigest.h>
#import "AFNetworking.h"

/**
 * AFNetworking结构及功能说明
 
 NSURLSession相关
 
 AFURLSessionManager
 AFHTTPSessionManager
 
 
 Serialization
 
 AFURLRequestSerialization
 > AFHTTPRequestSerializer
 > AFJSONRequestSerializer
 > AFPropertyListRequestSerializer
 AFURLResponseSerialization
 > AFHTTPResponseSerializer
 > AFJSONResponseSerializer
 > AFXMLParserResponseSerializer
 > AFXMLDocumentResponseSerializer (Mac OS X)
 > AFPropertyListResponseSerializer
 > AFImageResponseSerializer
 > AFCompoundResponseSerializer
 
 
 Additional Functionality
 
 AFSecurityPolicy
 AFNetworkReachabilityManager
 
 * 常见问题
 
 Q: The resource could not be loaded because the App Transport Security policy requires the use of a secure connection.即在iOS9中要求必须使用https协议
 A: 通过在Info.plist中添加NSAppTransportSecurity类型Dictionary,并在NSAppTransportSecurity下添加NSAllowsArbitraryLoads类型BOOL,设为YES,可以继续使用http协议
 
 Q: 在网络请求时,返回如下错误 Request failed: unacceptable content-type: text/html
 A: 在AFURLResponseSerialization.m文件第226行集合中加入@"text/html"即可
 */

@implementation Api

+ (void)requestWithMethod:(NSString *)method path:(NSString *)path params:(NSDictionary *)params success:(void (^)(id responseObject))success failed:(void (^)(NSError *error))failed
{
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    [dictionary setObject:@"1" forKey:@"phone_type"];
    [dictionary setObject:[[UIDevice currentDevice] localizedModel] forKey:@"phone_model"];
    [dictionary setObject:[[UIDevice currentDevice] systemVersion] forKey:@"os_version"];
    [dictionary setObject:[[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"] forKey:@"app_version"];
    [dictionary setObject:@"" forKey:@"user_id"];
    [dictionary setObject:jsonStr forKey:@"params"];
    [dictionary setObject:[self md5:[NSString stringWithFormat:@"%@%@%@%@%@%@cjwj", dictionary[@"phone_type"], dictionary[@"phone_model"], dictionary[@"os_version"], dictionary[@"app_version"], dictionary[@"user_id"], dictionary[@"params"]]] forKey:@"sign"];
    
    if ([[method uppercaseString] isEqualToString:@"POST"])
    {
        [[AFHTTPSessionManager manager] POST:[NSString stringWithFormat:@"%@%@", BASEURL, path] parameters:dictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            success(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            failed(error);
        }];
    }
    else
    {
        [[AFHTTPSessionManager manager] GET:[NSString stringWithFormat:@"%@%@", BASEURL, path] parameters:dictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            success(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            failed(error);
        }];
    }
}

+ (NSString *)md5:(NSString *)string
{
    const char *cStr = [string UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), result);
    NSMutableString *mutableStr = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
    {
        [mutableStr appendFormat:@"%02X", result[i]];
    }
    return [mutableStr lowercaseString];
}

@end
