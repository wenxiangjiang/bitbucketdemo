/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  Api.h
 项目：  SuperTeacher
 时间：  16/1/27
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import <Foundation/Foundation.h>


/**** 通用协议头 ****/
#define BASEURL @"http://123.59.164.185/" // 正式服务器


/**** 登录注册 ****/
#define API_LOGIN_URL @"index.php?app=api&mod=Login&act=login" // 登录
#define API_LOGOUT_URL @"index.php?app=api&mod=Login&act=logout" // 退出登录

#define API_SENDCAPTCHA_URL @"index.php?app=api&mod=User&act=sendCaptcha" // 发送短信验证码
#define API_CHECKCAPTCHA_URL @"index.php?app=api&mod=User&act=checkCaptcha" // 验证短信验证码
#define API_REGISTER_URL @"index.php?app=api&mod=Login&act=register" // 注册

#define API_CHANGEPASSWORD_URL @"index.php?app=api&mod=User&act=doModifyPassword" // 修改密码

#define API_SCHOOL_URL @"index.php?app=api&mod=User&act=getSchoolList" // 获取学校
#define API_AREA_URL @"index.php?app=api&mod=User&act=getAreaList" // 获取地区
#define API_GRADE_URL @"index.php?app=api&mod=User&act=getGradeList" // 获取年级


@interface Api : NSObject

/**
 * 参数1 -- 请求方法,GET POST
 * 参数2 -- 接口链接(类似index.php?app=api&mod=Login&act=login)
 * 参数3 -- 接口参数
 */
+ (void)requestWithMethod:(NSString *)method path:(NSString *)path params:(NSDictionary *)params success:(void (^)(id responseObject))success failed:(void (^)(NSError *error))failed;

/**
 * md5加密
 */
+ (NSString *)md5:(NSString *)string;

@end


