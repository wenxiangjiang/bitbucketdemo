/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  LeftViewController.m
 项目：  SuperTeacher
 时间：  16/1/21
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import "LeftViewController.h"
#import "AppDelegate.h"
#import "IIViewDeckController.h"
#import "PersonalProfileViewController.h"
#import "PurchaseCourseViewController.h"
#import "LearningPortfolioViewController.h"
#import "PurchaseHistoryViewController.h"
#import "SettingViewController.h"

@interface LeftViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataSource;

@end

@implementation LeftViewController

#pragma mark - Lifecycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.tableView];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"loly";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == NULL)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor clearColor];
    }
    
    cell.imageView.image = [UIImage imageNamed:self.dataSource[indexPath.row][@"image"]];
    cell.textLabel.text = self.dataSource[indexPath.row][@"title"];
    cell.textLabel.font = [UIFont systemFontOfSize:15.0f];
    cell.textLabel.textColor = LPHEX(0x404040);
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 150.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 275, 150)];
    view.backgroundColor = LPDefaultColorNormal;
    
    UIImageView *faceImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 45, 60, 60)];
    faceImageView.image = LPDefaultFaceImage;
    faceImageView.layer.masksToBounds = YES;
    faceImageView.layer.cornerRadius = 30;
    [view addSubview:faceImageView];
    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 45, 140, 30)];
    nameLabel.text = @"李明";
    nameLabel.font = [UIFont systemFontOfSize:15.0f];
    nameLabel.textColor = [UIColor whiteColor];
    [view addSubview:nameLabel];
    
    UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 75, 140, 30)];
    phoneLabel.text = @"131****2341";
    phoneLabel.font = [UIFont systemFontOfSize:15.0f];
    phoneLabel.textColor = [UIColor whiteColor];
    [view addSubview:phoneLabel];
    
    UIImageView *moreImageView = [[UIImageView alloc] initWithFrame:CGRectMake(240, 67.5, 15, 15)];
    moreImageView.image = [UIImage imageNamed:@"My_more"];
    [view addSubview:moreImageView];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.frame = CGRectMake(0, 0, 225, 150);
    [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:button];
    
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.deckController closeLeftViewAnimated:YES];
    
    switch (indexPath.row)
    {
        case 0:
        {
            PurchaseCourseViewController *purchaseCourseVC = [[PurchaseCourseViewController alloc] init];
            [self presentViewController:[[UINavigationController alloc] initWithRootViewController:purchaseCourseVC] animated:YES completion:^{
            }];
        }
            break;
        case 1:
        {
            LearningPortfolioViewController *learningPortfolioVC = [[LearningPortfolioViewController alloc] init];
            [self presentViewController:[[UINavigationController alloc] initWithRootViewController:learningPortfolioVC] animated:YES completion:^{
            }];
        }
            break;
        case 2:
        {
            PurchaseHistoryViewController *purchaseHistoryVC = [[PurchaseHistoryViewController alloc] init];
            [self presentViewController:[[UINavigationController alloc] initWithRootViewController:purchaseHistoryVC] animated:YES completion:^{
            }];
        }
            break;
        case 3:
        {
            SettingViewController *settingVC = [[SettingViewController alloc] init];
            [self presentViewController:[[UINavigationController alloc] initWithRootViewController:settingVC] animated:YES completion:^{
            }];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - CustomDelegate
#pragma mark - Event and Responce
- (void)buttonClicked:(UIButton *)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.deckController closeLeftViewAnimated:YES];
    PersonalProfileViewController *personalProfileVC = [[PersonalProfileViewController alloc] init];
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:personalProfileVC] animated:YES completion:^{
    }];
}

#pragma mark - Notification Methods
#pragma mark - Private Methods
#pragma mark - Setters and Getters
- (UITableView *)tableView
{
    if (!_tableView)
    {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 275, [[UIScreen mainScreen] bounds].size.height) style:UITableViewStyleGrouped];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//        _tableView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0); // 如果在外面使用了UINavigation且隐藏了NavigationBar,上边会自动留白20,调整回来
        _tableView.dataSource = self;
        _tableView.delegate = self;
    }
    return _tableView;
}

- (NSMutableArray *)dataSource
{
    if (!_dataSource)
    {
        _dataSource = [NSMutableArray arrayWithArray:@[@{@"image": @"My_PurchaseCourse", @"title": @"购买课程"},
                                                       @{@"image": @"My_LearningPortfolio", @"title": @"学习档案"},
                                                       @{@"image": @"My_PurchaseHistory", @"title": @"购买记录"},
                                                       @{@"image": @"My_Setting", @"title": @"设置"}]];
    }
    return _dataSource;
}

#pragma mark - Dealloc
- (void)dealloc
{
    LPLog(@"Left dealloc");
    self.tableView = nil;
    self.dataSource = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
