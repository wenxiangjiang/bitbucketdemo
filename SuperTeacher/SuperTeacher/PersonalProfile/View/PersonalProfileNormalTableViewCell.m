/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  PersonalProfileNormalTableViewCell.m
 项目：  SuperTeacher
 时间：  16/1/21
 所有：  Copyright (c) 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import "PersonalProfileNormalTableViewCell.h"

@implementation PersonalProfileNormalTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 80, 48-LPSeparatorHeight)];
        self.titleLabel.textColor = LPHEX(0x404040);
        self.titleLabel.font = [UIFont systemFontOfSize:15.0f];
        [self.contentView addSubview:self.titleLabel];
        
        self.detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 0, [[UIScreen mainScreen] bounds].size.width-130, 48-LPSeparatorHeight)];
        self.detailLabel.textColor = LPHEX(0x9e9e9e);
        self.detailLabel.font = [UIFont systemFontOfSize:15.0f];
        self.detailLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:self.detailLabel];
        
        self.lineView = [[UIView alloc] initWithFrame:CGRectMake(20, 48-LPSeparatorHeight, [[UIScreen mainScreen] bounds].size.width-20, LPSeparatorHeight)];
        self.lineView.backgroundColor = LPSeparatorColor;
        [self.contentView addSubview:self.lineView];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
    self.lineView.backgroundColor = LPSeparatorColor;
}

@end
