/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  PersonalProfileDetailViewController.m
 项目：  SuperTeacher
 时间：  16/1/21
 所有：  Copyright (c) 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import "PersonalProfileDetailViewController.h"

@interface PersonalProfileDetailViewController ()

@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UIButton *submitButton;

@end

@implementation PersonalProfileDetailViewController

#pragma mark - Lifecycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    switch (self.personalProfileDetailType)
    {
        case PersonalProfileDetailTypeRealName:
        {
            self.title = @"真实姓名";
            self.textField.text = [LoginHelper getRealName];
            self.textField.keyboardType = UIKeyboardTypeNamePhonePad;
        }
            break;
        case PersonalProfileDetailTypePhone:
        {
            self.title = @"手机号";
            self.textField.text = [LoginHelper getPhoneNumber];
            self.textField.keyboardType = UIKeyboardTypeNumberPad;
        }
            break;
        case PersonalProfileDetailTypeFamilyPhone:
        {
            self.title = @"家长手机号";
            self.textField.text = [LoginHelper getFamilyPhoneNumber];
            self.textField.keyboardType = UIKeyboardTypeNumberPad;
        }
            break;
            
        default:
            break;
    }
    
    [self.view addSubview:self.textField];
    [self.view addSubview:self.lineView];
    [self.view addSubview:self.submitButton];
}

#pragma mark - UITableViewDataSource
#pragma mark - UITableViewDelegate
#pragma mark - CustomDelegate
#pragma mark - Event and Responce

- (void)textFieldValueChanged:(UITextField *)textField
{
    if (textField.text.length > 0)
    {
        self.submitButton.enabled = YES;
    }
    else
    {
        self.submitButton.enabled = NO;
    }
}

- (void)buttonClicked:(UIButton *)button
{
    [LoginHelper saveRealName:self.textField.text];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AAA" object:nil userInfo:@{@"realName": self.textField.text}];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Notification Methods
#pragma mark - Private Methods
#pragma mark - Setters and Getters
- (UITextField *)textField
{
    if (!_textField)
    {
        _textField = [[UITextField alloc] initWithFrame:CGRectMake(20, 88, [[UIScreen mainScreen] bounds].size.width-40, 40)];
        _textField.font = [UIFont systemFontOfSize:15.0f];
        _textField.tintColor = LPDefaultColorNormal;
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textField.autocorrectionType = UITextAutocorrectionTypeNo;
        _textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        [_textField becomeFirstResponder];
        [_textField addTarget:self action:@selector(textFieldValueChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    return _textField;
}

- (UIView *)lineView
{
    if (!_lineView)
    {
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(20, 128, [[UIScreen mainScreen] bounds].size.width-40, LPSeparatorHeight)];
        _lineView.backgroundColor = LPSeparatorColor;
    }
    return _lineView;
}

- (UIButton *)submitButton
{
    if (!_submitButton)
    {
        _submitButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _submitButton.frame = CGRectMake(20, 170, [[UIScreen mainScreen] bounds].size.width-40, 44);
        _submitButton.layer.masksToBounds = YES;
        _submitButton.layer.cornerRadius = 5;
        _submitButton.titleLabel.font = [UIFont systemFontOfSize:17.0f];
        _submitButton.enabled = NO;
        [_submitButton setTitle:@"保存" forState:UIControlStateNormal];
        [_submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_submitButton setBackgroundImage:[[LPHelper shareLPHelper] imageWithColor:LPDefaultColorNormal] forState:UIControlStateNormal];
        [_submitButton setBackgroundImage:[[LPHelper shareLPHelper] imageWithColor:LPDefaultColorDisable] forState:UIControlStateDisabled];
        [_submitButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _submitButton;
}

#pragma mark - Dealloc
- (void)dealloc
{
    LPLog(@"PersonalProfileDetailViewController dealloc");
    self.textField = nil;
    self.lineView = nil;
    self.submitButton = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
