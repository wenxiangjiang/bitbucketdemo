/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  PersonalProfileDetailViewController.h
 项目：  SuperTeacher
 时间：  16/1/21
 所有：  Copyright (c) 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, PersonalProfileDetailType)
{
    PersonalProfileDetailTypeRealName = 0,
    PersonalProfileDetailTypePhone,
    PersonalProfileDetailTypeFamilyPhone
};

@interface PersonalProfileDetailViewController : UIViewController

@property (nonatomic, assign) PersonalProfileDetailType personalProfileDetailType;

@end
