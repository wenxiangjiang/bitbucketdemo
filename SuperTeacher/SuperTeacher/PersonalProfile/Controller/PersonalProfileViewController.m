/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  PersonalProfileViewController.m
 项目：  SuperTeacher
 时间：  16/1/21
 所有：  Copyright (c) 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import "PersonalProfileViewController.h"
#import "PersonalProfileNormalTableViewCell.h"
#import "PersonalProfileImageTableViewCell.h"
#import "PersonalProfileDetailViewController.h"

@interface PersonalProfileViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *keyDataSource;
@property (nonatomic, strong) NSMutableArray *valueDataSource;

@end

@implementation PersonalProfileViewController

#pragma mark - Lifecycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(realNameChange:) name:@"AAA" object:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"个人信息";
    self.navigationController.navigationBar.tintColor = LPDefaultColorNormal;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: LPHEX(0x666666), NSFontAttributeName: [UIFont systemFontOfSize:18.0f]};
    self.navigationController.navigationBar.translucent = YES;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(leftBarButtonItemClicked:)];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationController.navigationBar setBackgroundImage:[[LPHelper shareLPHelper] imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    
    [self.view addSubview:self.tableView];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.keyDataSource count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.keyDataSource[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath isEqual:[NSIndexPath indexPathForRow:0 inSection:0]])
    {
        static NSString *identifier = @"PersonalProfileImageCell";
        PersonalProfileImageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == NULL)
        {
            cell = [[PersonalProfileImageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        cell.titleLabel.text = self.keyDataSource[indexPath.section][indexPath.row];
        cell.faceImageView.image = LPDefaultFaceImage;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        
        return cell;
    }
    else
    {
        static NSString *identifier = @"PersonalProfileNormalCell";
        PersonalProfileNormalTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == NULL)
        {
            cell = [[PersonalProfileNormalTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        cell.titleLabel.text = self.keyDataSource[indexPath.section][indexPath.row];
        cell.detailLabel.text = self.valueDataSource[indexPath.section][indexPath.row];
        if ([indexPath isEqual:[NSIndexPath indexPathForRow:1 inSection:0]])
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        }
        
        return cell;
    }
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath isEqual:[NSIndexPath indexPathForRow:0 inSection:0]])
    {
        return 72.0f;
    }
    return 48.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 24.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [ProgressHelper showTextHUDAddedTo:self.view text:@"这里暂时禁止点击,功能在完善资料完成" animated:YES];
    
//    if (indexPath.section == 0)
//    {
//        switch (indexPath.row)
//        {
//            case 0:
//            {
//                [LPActionSheetView showActionSheetWithTitle:@"" cancelButtonTitle:@"取消" destructiveButtonTitle:@"" otherButtonTitles:@[@"拍照", @"从手机相册选择"] handler:^(LPActionSheetView *actionSheetView, NSInteger index) {
//                    LPLog(@"%lu", index);
//                }];
//            }
//                break;
//            case 1:
//            {
//                // 不可点击
//            }
//                break;
//            case 2:
//            {
//                // 真实姓名
//                PersonalProfileDetailViewController *personalProfileDetailVC = [[PersonalProfileDetailViewController alloc] init];
//                personalProfileDetailVC.personalProfileDetailType = PersonalProfileDetailTypeRealName;
//                [self.navigationController pushViewController:personalProfileDetailVC animated:YES];
//            }
//                break;
//            case 3:
//            {
//                // 手机号
////                PersonalProfileDetailViewController *personalProfileDetailVC = [[PersonalProfileDetailViewController alloc] init];
////                personalProfileDetailVC.personalProfileDetailType = PersonalProfileDetailTypePhone;
////                [self.navigationController pushViewController:personalProfileDetailVC animated:YES];
//            }
//                break;
//            case 4:
//            {
//                // 家长手机号
////                PersonalProfileDetailViewController *personalProfileDetailVC = [[PersonalProfileDetailViewController alloc] init];
////                personalProfileDetailVC.personalProfileDetailType = PersonalProfileDetailTypeFamilyPhone;
////                [self.navigationController pushViewController:personalProfileDetailVC animated:YES];
//            }
//                break;
//                
//            default:
//                break;
//        }
//    }
//    else
//    {
//        switch (indexPath.row)
//        {
//            case 0:
//            {
//                
//            }
//                break;
//            case 1:
//            {
//                [LPPickerView showPickerViewWithTitle:@"请选择性别" dataTitles:@[@"男", @"女"] handler:^(LPPickerView *pickerView, NSInteger index) {
//                    LPLog(@"%lu", index);
//                }];
//            }
//                break;
//            case 2:
//            {
//                
//            }
//                break;
//            case 3:
//            {
//                
//            }
//                break;
//            case 4:
//            {
//                [LPPickerView showPickerViewWithTitle:@"请选择年级" dataTitles:@[@"一年级", @"二年级", @"三年级"] handler:^(LPPickerView *pickerView, NSInteger index) {
//                    LPLog(@"%lu", index);
//                }];
//            }
//                break;
//                
//            default:
//                break;
//        }
//    }
}

#pragma mark - CustomDelegate
#pragma mark - Event and Responce
- (void)leftBarButtonItemClicked:(UIBarButtonItem *)barButtonItem
{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

- (void)realNameChange:(NSNotification *)notification
{
    NSMutableArray *arr = [NSMutableArray arrayWithArray:self.valueDataSource[0]];
    [arr replaceObjectAtIndex:2 withObject:notification.userInfo[@"realName"]];
    [self.valueDataSource replaceObjectAtIndex:0 withObject:arr];
    [self.tableView reloadData];
}

#pragma mark - Notification Methods
#pragma mark - Private Methods
#pragma mark - Setters and Getters
- (UITableView *)tableView
{
    if (!_tableView)
    {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-64) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.dataSource = self;
        _tableView.delegate = self;
    }
    return _tableView;
}

- (NSMutableArray *)keyDataSource
{
    if (!_keyDataSource)
    {
        _keyDataSource = [NSMutableArray arrayWithArray:@[@[@"头像", @"用户名", @"真实姓名", @"手机号", @"家长手机号"],
                                                          @[@"生日", @"性别", @"地区", @"学校", @"年级"]]];
    }
    return _keyDataSource;
}

- (NSMutableArray *)valueDataSource
{
    if (!_valueDataSource)
    {
        _valueDataSource = [NSMutableArray arrayWithArray:@[@[@"头像", @"用户名", @"真实姓名", @"手机号", @"家长手机号"],
                                                          @[@"生日", @"性别", @"地区", @"学校", @"年级"]]];
    }
    return _valueDataSource;
}

#pragma mark - Dealloc
- (void)dealloc
{
    LPLog(@"PersonalProfile dealloc");
    self.tableView = nil;
    self.keyDataSource = nil;
    self.valueDataSource = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AAA" object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
