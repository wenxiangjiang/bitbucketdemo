/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  LoginViewController.m
 项目：  SuperTeacher
 时间：  16/1/21
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import "LoginViewController.h"
#import "AppDelegate.h"
#import "IIViewDeckController.h"
#import "CViewController.h"
#import "LeftViewController.h"
#import "RetrieveFirstViewController.h"

@interface LoginViewController ()

@property (nonatomic, strong) UIImageView *logoImageView;
@property (nonatomic, strong) UITextField *firstTextField;
@property (nonatomic, strong) UITextField *secondTextField;
@property (nonatomic, strong) UIView *firstLineView;
@property (nonatomic, strong) UIView *secondLineView;
@property (nonatomic, strong) UIButton *showButton;
@property (nonatomic, strong) UIButton *submitButton;
@property (nonatomic, strong) UIButton *retrieveButton;

@end

@implementation LoginViewController

#pragma mark - Lifecycle
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBar.tintColor = LPDefaultColorNormal;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: LPHEX(0x666666), NSFontAttributeName: [UIFont systemFontOfSize:18.0f]};
    self.navigationController.navigationBar.translucent = YES;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemClicked:)];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationController.navigationBar setBackgroundImage:[[LPHelper shareLPHelper] imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    
    [self.view addSubview:self.logoImageView];
    [self.view addSubview:self.firstTextField];
    [self.view addSubview:self.secondTextField];
    [self.view addSubview:self.firstLineView];
    [self.view addSubview:self.secondLineView];
    [self.view addSubview:self.showButton];
    [self.view addSubview:self.submitButton];
    [self.view addSubview:self.retrieveButton];
}

#pragma mark - UITableViewDataSource
#pragma mark - UITableViewDelegate
#pragma mark - CustomDelegate
#pragma mark - Event and Responce
- (void)leftBarButtonItemClicked:(UIBarButtonItem *)barButtonItem
{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

- (void)buttonClicked:(UIButton *)button
{
    LPMethod();
    switch (button.tag)
    {
        case 1:
        {
            if (button.selected)
            {
                self.secondTextField.secureTextEntry = YES;
            }
            else
            {
                self.secondTextField.secureTextEntry = NO;
            }
            
            // 解决secureTextEntry在切换过程中导致光标错位的问题
            NSString *tempString = self.secondTextField.text;
            self.secondTextField.text = @"";
            self.secondTextField.text = tempString;
            
            button.selected = !button.selected;
        }
            break;
        case 2:
        {
            [self.firstTextField resignFirstResponder];
            [self.secondTextField resignFirstResponder];
            
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            [params setObject:self.firstTextField.text forKey:@"uname"];
            [params setObject:self.secondTextField.text forKey:@"upwd"];

            [Api requestWithMethod:@"POST" path:API_LOGIN_URL params:params success:^(id responseObject) {
                LPLog(@"%@", responseObject);
                if ([responseObject[@"code"] intValue] == 0)
                {
                    [LoginHelper saveUserName:responseObject[@"data"][@"uname"]];
                    [LoginHelper saveUserID:responseObject[@"data"][@"uid"]];
                    
                    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    
                    LeftViewController *leftVC = [[LeftViewController alloc] init];
                    CViewController *cVC = [[CViewController alloc] init];
                    
                    IIViewDeckController* deckController =  [[IIViewDeckController alloc] initWithCenterViewController:[[UINavigationController alloc] initWithRootViewController:cVC] leftViewController:leftVC];;
                    appDelegate.deckController = deckController;
                    deckController.leftSize = [[UIScreen mainScreen] bounds].size.width - 275.0f;
                    deckController.openSlideAnimationDuration = 0.3f;
                    deckController.closeSlideAnimationDuration = 0.3f;
                    deckController.bounceDurationFactor = 0.3f;
                    deckController.centerhiddenInteractivity = IIViewDeckCenterHiddenNotUserInteractiveWithTapToClose;
                    appDelegate.window.rootViewController = deckController;
                    [appDelegate.window makeKeyAndVisible];
                }
                else
                {
                    [ProgressHelper showTextHUDAddedTo:self.view text:responseObject[@"msg"] animated:YES];
                }
            } failed:^(NSError *error) {
                [ProgressHelper showTextHUDAddedTo:self.view text:[error localizedDescription] animated:YES];
            }];
        }
            break;
        case 3:
        {
            [self.firstTextField resignFirstResponder];
            [self.secondTextField resignFirstResponder];
            
            RetrieveFirstViewController *retrieveFirstVC = [[RetrieveFirstViewController alloc] init];
            [self.navigationController pushViewController:retrieveFirstVC animated:YES];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Notification Methods
#pragma mark - Private Methods
#pragma mark - Setters and Getters
- (UIImageView *)logoImageView
{
    if (!_logoImageView)
    {
        _logoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width-160)/2, 64, 160, 35)];
        _logoImageView.image = [UIImage imageNamed:@"Login_logo"];
    }
    return _logoImageView;
}

- (UITextField *)firstTextField
{
    if (!_firstTextField)
    {
        _firstTextField = [[UITextField alloc] initWithFrame:CGRectMake(20, 113, [[UIScreen mainScreen] bounds].size.width-40, 48)];
        _firstTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"请输入账号" attributes:@{NSForegroundColorAttributeName: LPHEX(0xcacaca)}];
        _firstTextField.font = [UIFont systemFontOfSize:15.0f];
        _firstTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        _firstTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _firstTextField.tintColor = LPDefaultColorNormal;
        
    }
    return _firstTextField;
}

- (UITextField *)secondTextField
{
    if (!_secondTextField)
    {
        _secondTextField = [[UITextField alloc] initWithFrame:CGRectMake(20, 170, [[UIScreen mainScreen] bounds].size.width-90, 48)];
        _secondTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"请输入密码" attributes:@{NSForegroundColorAttributeName: LPHEX(0xcacaca)}];
        _secondTextField.font = [UIFont systemFontOfSize:15.0f];
        _secondTextField.secureTextEntry = YES;
        _secondTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        _secondTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _secondTextField.tintColor = LPDefaultColorNormal;
    }
    return _secondTextField;
}

- (UIView *)firstLineView
{
    if (!_firstLineView)
    {
        _firstLineView = [[UIView alloc] initWithFrame:CGRectMake(20, 161, [[UIScreen mainScreen] bounds].size.width-40, LPSeparatorHeight)];
        _firstLineView.backgroundColor = LPSeparatorColor;
    }
    return _firstLineView;
}

- (UIView *)secondLineView
{
    if (!_secondLineView)
    {
        _secondLineView = [[UIView alloc] initWithFrame:CGRectMake(20, 218, [[UIScreen mainScreen] bounds].size.width-40, LPSeparatorHeight)];
        _secondLineView.backgroundColor = LPSeparatorColor;
    }
    return _secondLineView;
}

- (UIButton *)showButton
{
    if (!_showButton)
    {
        _showButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _showButton.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width-60, 184, 40, 20);
        _showButton.layer.masksToBounds = YES;
        _showButton.layer.cornerRadius = 5;
        _showButton.titleLabel.font = [UIFont systemFontOfSize:15.0f];
        _showButton.tag = 1;
        [_showButton setTitle:@"显示" forState:UIControlStateNormal];
        [_showButton setTitle:@"隐藏" forState:UIControlStateSelected];
        [_showButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_showButton setBackgroundImage:[[LPHelper shareLPHelper] imageWithColor:LPDefaultColorNormal] forState:UIControlStateNormal];
        [_showButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _showButton;
}

- (UIButton *)submitButton
{
    if (!_submitButton)
    {
        _submitButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _submitButton.frame = CGRectMake(20, 250, [[UIScreen mainScreen] bounds].size.width-40, 44);
        _submitButton.layer.masksToBounds = YES;
        _submitButton.layer.cornerRadius = 5;
        _submitButton.titleLabel.font = [UIFont systemFontOfSize:17.0f];
        _submitButton.tag = 2;
        [_submitButton setTitle:@"登录" forState:UIControlStateNormal];
        [_submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_submitButton setBackgroundImage:[[LPHelper shareLPHelper] imageWithColor:LPDefaultColorNormal] forState:UIControlStateNormal];
        [_submitButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _submitButton;
}

- (UIButton *)retrieveButton
{
    if (!_retrieveButton)
    {
        _retrieveButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _retrieveButton.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width-110, 310, 90, 20);
        _retrieveButton.titleLabel.font = [UIFont systemFontOfSize:12.0f];
        _retrieveButton.tag = 3;
        [_retrieveButton setTitle:@"登录遇到问题？" forState:UIControlStateNormal];
        [_retrieveButton setTitleColor:LPHEX(0x9e9e9e) forState:UIControlStateNormal];
        [_retrieveButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _retrieveButton;
}

#pragma mark - Dealloc
- (void)dealloc
{
    LPLog(@"Login dealloc");
    self.logoImageView = nil;
    self.firstTextField = nil;
    self.secondTextField = nil;
    self.firstLineView = nil;
    self.secondLineView = nil;
    self.showButton = nil;
    self.submitButton = nil;
    self.retrieveButton = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
