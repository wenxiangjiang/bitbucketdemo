/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  UserInformationViewController.m
 项目：  SuperTeacher
 时间：  16/1/21
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import "UserInformationViewController.h"
#import "UserInformationTextFieldTableViewCell.h"
#import "UserModel.h"
#import "SchoolViewController.h"
#import "AreaViewController.h"

@interface UserInformationViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *keyDataSource;
@property (nonatomic, strong) NSMutableArray *valueDataSource;
@property (nonatomic, strong) UserModel *userModel;

@end

@implementation UserInformationViewController

#pragma mark - Lifecycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"完善资料";
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [self.view addSubview:self.tableView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(schoolChanged:) name:@"SCHOOLCHANGED" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(areaChanged:) name:@"AREACHANGED" object:nil];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.keyDataSource count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.keyDataSource[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"UserInformationTextFieldTableViewCell";
    UserInformationTextFieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == NULL)
    {
        cell = [[UserInformationTextFieldTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    cell.titleLabel.text = self.keyDataSource[indexPath.section][indexPath.row];
    cell.textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.valueDataSource[indexPath.section][indexPath.row] attributes:@{NSForegroundColorAttributeName: LPHEX(0x9e9e9e)}];
    if ([indexPath isEqual:[NSIndexPath indexPathForRow:0 inSection:0]] || [indexPath isEqual:[NSIndexPath indexPathForRow:2 inSection:1]])
    {
        cell.textField.enabled = YES;
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    else
    {
        cell.textField.enabled = NO;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    }
    
    switch (indexPath.section)
    {
        case 0:
        {
            
        }
            break;
        case 1:
        {
            switch (indexPath.row)
            {
                case 0:
                {
                    cell.textField.text = self.userModel.sexString;
                }
                    break;
                case 1:
                {
                    cell.textField.text = self.userModel.birthdayString;
                }
                    break;
                case 2:
                {
                    
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
        case 2:
        {
            switch (indexPath.row)
            {
                case 0:
                {
                    cell.textField.text = self.userModel.areaString;
                }
                    break;
                case 1:
                {
                    cell.textField.text = self.userModel.schoolString;
                }
                    break;
                case 2:
                {
                    cell.textField.text = self.userModel.gradeString;
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        default:
            break;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 48.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 24.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 2)
    {
        return 108.0f;
    }
    return 0.01f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0)
    {
        switch (indexPath.row)
        {
            case 0:
            {
                
            }
                break;
                
            default:
                break;
        }
    }
    else if (indexPath.section == 1)
    {
        switch (indexPath.row)
        {
            case 0:
            {
                [LPPickerView showPickerViewWithTitle:@"请选择性别" dataTitles:@[@"男", @"女"] handler:^(LPPickerView *pickerView, NSInteger index) {
                    LPLog(@"%lu", index);
                    if (index == 0)
                    {
                        self.userModel.sexString = @"男";
                        self.userModel.sexID = @"1";
                    }
                    else
                    {
                        self.userModel.sexString = @"女";
                        self.userModel.sexID = @"2";
                    }
                    [self.tableView reloadData];
                }];
            }
                break;
            case 1:
            {
                [LPDatePickerView showDatePickerViewWithTitle:@"请选择出生日期" handler:^(LPDatePickerView *dataPickerView, NSDate *date) {
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                    NSString *dateTime =  [dateFormatter stringFromDate:date];
                    
                    self.userModel.birthdayString = dateTime;
                    self.userModel.birthdayID = [NSString stringWithFormat:@"%ld", (NSInteger)[date timeIntervalSince1970]];
                    [self.tableView reloadData];
                }];
            }
                break;
            case 2:
            {
                
            }
                break;
                
            default:
                break;
        }
    }
    else
    {
        switch (indexPath.row)
        {
            case 0:
            {
                AreaViewController *areaVC = [[AreaViewController alloc] init];
                [self.navigationController pushViewController:areaVC animated:YES];
            }
                break;
            case 1:
            {
                SchoolViewController *schoolVC = [[SchoolViewController alloc] init];
                [self.navigationController pushViewController:schoolVC animated:YES];
            }
                break;
            case 2:
            {
                NSMutableDictionary *params = [NSMutableDictionary dictionary];
                
                [Api requestWithMethod:@"POST" path:API_GRADE_URL params:params success:^(id responseObject) {
                    LPLog(@"%@", responseObject);
                    if ([responseObject[@"code"] intValue] == 0)
                    {
                        NSMutableArray *nameArray = [NSMutableArray array];
                        for (NSDictionary *dic in responseObject[@"data"])
                        {
                            [nameArray addObject:dic[@"name"]];
                        }
                        [LPPickerView showPickerViewWithTitle:@"请选择年级" dataTitles:nameArray handler:^(LPPickerView *pickerView, NSInteger index) {
                            LPLog(@"%lu", index);
                            self.userModel.gradeString = responseObject[@"data"][index][@"name"];
                            self.userModel.gradeID = responseObject[@"data"][index][@"id"];
                            [self.tableView reloadData];
                        }];
                    }
                    else
                    {
                        [ProgressHelper showTextHUDAddedTo:self.view text:responseObject[@"msg"] animated:YES];
                    }
                } failed:^(NSError *error) {
                    [ProgressHelper showTextHUDAddedTo:self.view text:[error localizedDescription] animated:YES];
                }];
            }
                break;
                
            default:
                break;
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == 2)
    {
        UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(20, 32, [[UIScreen mainScreen] bounds].size.width-40, 44);
        button.layer.masksToBounds = YES;
        button.layer.cornerRadius = 5;
        button.titleLabel.font = [UIFont systemFontOfSize:17.0f];
        [button setTitle:@"提交资料" forState:UIControlStateNormal];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [button setBackgroundImage:[[LPHelper shareLPHelper] imageWithColor:LPDefaultColorNormal] forState:UIControlStateNormal];
        [button setBackgroundImage:[[LPHelper shareLPHelper] imageWithColor:LPDefaultColorDisable] forState:UIControlStateDisabled];
        [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:button];
        
        return view;
    }
    return nil;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}

#pragma mark - Event and Responce
- (void)buttonClicked:(UIButton *)button
{
    NSLog(@"-------");
    
    [ProgressHelper showTextHUDAddedTo:self.view text:@"接口不完善,模拟失败" animated:YES];
}

- (void)schoolChanged:(NSNotification *)notification
{
    self.userModel.schoolString = notification.userInfo[@"name"];
    self.userModel.schoolID = notification.userInfo[@"id"];
    [self.tableView reloadData];
}

- (void)areaChanged:(NSNotification *)notification
{
    self.userModel.areaString = notification.userInfo[@"name"];
    self.userModel.areaID = notification.userInfo[@"id"];
    [self.tableView reloadData];
}

#pragma mark - Notification Methods
#pragma mark - Private Methods
#pragma mark - Setters and Getters
- (UITableView *)tableView
{
    if (!_tableView)
    {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-64) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.dataSource = self;
        _tableView.delegate = self;
    }
    return _tableView;
}

- (NSMutableArray *)keyDataSource
{
    if (!_keyDataSource)
    {
        _keyDataSource = [NSMutableArray arrayWithArray:@[@[@"真实姓名"],
                                                          @[@"性别", @"出生日期", @"家长手机号"],
                                                          @[@"地区", @"学校", @"年级"]]];
    }
    return _keyDataSource;
}

- (NSMutableArray *)valueDataSource
{
    if (!_valueDataSource)
    {
        _valueDataSource = [NSMutableArray arrayWithArray:@[@[@"请输入学生的姓名"],
                                                            @[@"请选择性别", @"请选择出生日期", @"请输入家长的手机号"],
                                                            @[@"请选择所在省市", @"请选择学校", @"请选择年级"]]];
    }
    return _valueDataSource;
}

- (UserModel *)userModel
{
    if (!_userModel)
    {
        _userModel = [[UserModel alloc] init];
    }
    return _userModel;
}

#pragma mark - Dealloc
- (void)dealloc
{
    LPLog(@"PersonalProfile dealloc");
    self.tableView = nil;
    self.keyDataSource = nil;
    self.valueDataSource = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SCHOOLCHANGED" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AREACHANGED" object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
