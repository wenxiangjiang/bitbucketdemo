/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  SchoolViewController.m
 项目：  SuperTeacher
 时间：  16/1/21
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import "SchoolViewController.h"

@interface SchoolViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataSource;

@end

@implementation SchoolViewController

#pragma mark - Lifecycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"选择学校";
    
    [self.view addSubview:self.tableView];
    [self loadDataSource];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"loly";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == NULL)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(15, 48-LPSeparatorHeight, [[UIScreen mainScreen] bounds].size.width-15, LPSeparatorHeight)];
        lineView.backgroundColor = LPSeparatorColor;
        [cell.contentView addSubview:lineView];
    }
    
    cell.textLabel.textColor = LPHEX(0x404040);;
    cell.textLabel.font = [UIFont systemFontOfSize:15];
    cell.textLabel.text = self.dataSource[indexPath.row][@"name"];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 48.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 24.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SCHOOLCHANGED" object:nil userInfo:@{@"name": self.dataSource[indexPath.row][@"name"], @"id": self.dataSource[indexPath.row][@"id"]}];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - CustomDelegate
#pragma mark - Event and Responce
#pragma mark - Notification Methods
#pragma mark - Private Methods
- (void)loadDataSource
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [Api requestWithMethod:@"POST" path:API_SCHOOL_URL params:params success:^(id responseObject) {
        LPLog(@"%@", responseObject);
        if ([responseObject[@"code"] intValue] == 0)
        {
            [self.dataSource addObjectsFromArray:responseObject[@"data"]];
            [self.tableView reloadData];
        }
        else
        {
            [ProgressHelper showTextHUDAddedTo:self.view text:responseObject[@"msg"] animated:YES];
        }
    } failed:^(NSError *error) {
        [ProgressHelper showTextHUDAddedTo:self.view text:[error localizedDescription] animated:YES];
    }];
}

#pragma mark - Setters and Getters
- (UITableView *)tableView
{
    if (!_tableView)
    {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-64) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.dataSource = self;
        _tableView.delegate = self;
    }
    return _tableView;
}

- (NSMutableArray *)dataSource
{
    if (!_dataSource)
    {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

#pragma mark - Dealloc
- (void)dealloc
{
    NSLog(@"SchoolViewController dealloc");
    self.tableView = nil;
    self.dataSource = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
