/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  GuideViewController.m
 项目：  SuperTeacher
 时间：  16/1/21
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import "GuideViewController.h"
#import "RegisterFirstViewController.h"
#import "LoginViewController.h"

@interface GuideViewController ()

@property (nonatomic, strong) UIImageView *backImageView;
@property (nonatomic, strong) UIButton *registerButton;
@property (nonatomic, strong) UIButton *loginButton;
//@property (nonatomic, strong) CALayer *theLayer;

@end

@implementation GuideViewController

#pragma mark - Lifecycle
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.backImageView];
    [self.view addSubview:self.registerButton];
    [self.view addSubview:self.loginButton];
    
//    self.theLayer = [CALayer layer];
//    self.theLayer.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.width-1000)/2,
//                                     ([[UIScreen mainScreen] bounds].size.height-1000)/2,
//                                     1000,
//                                     1000);
//    self.theLayer.backgroundColor = [UIColor clearColor].CGColor;
//    self.theLayer.cornerRadius = 1000/2;
//    self.theLayer.borderColor = [UIColor grayColor].CGColor;
//    self.theLayer.borderWidth = 1000/2-10;
//    [self.view.layer addSublayer:self.theLayer];
//    
//    CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
//    scaleAnimation.toValue = @(50.0f);
//    scaleAnimation.duration = 1.0f;
//    scaleAnimation.fillMode = kCAFillModeForwards;
//    scaleAnimation.delegate = self;
//    [self.theLayer addAnimation:scaleAnimation forKey:@"transform.scale"];
}

#pragma mark - UITableViewDataSource
#pragma mark - UITableViewDelegate
#pragma mark - CAAnimationDelegate
//- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
//{
//    [self.theLayer removeFromSuperlayer];
//}

#pragma mark - Event and Responce
- (void)buttonClicked:(UIButton *)button
{
    switch (button.tag)
    {
        case 1:
        {
            RegisterFirstViewController *registerFirstVC = [[RegisterFirstViewController alloc] init];
            [self presentViewController:[[UINavigationController alloc] initWithRootViewController:registerFirstVC] animated:YES completion:^{
            }];
        }
            break;
        case 2:
        {
            LoginViewController *loginVC = [[LoginViewController alloc] init];
            [self presentViewController:[[UINavigationController alloc] initWithRootViewController:loginVC] animated:YES completion:^{
            }];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Notification Methods
#pragma mark - Private Methods
#pragma mark - Setters and Getters
- (UIImageView *)backImageView
{
    if (!_backImageView)
    {
        _backImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)];
        _backImageView.image = [UIImage imageNamed:@"Guide_Back"];
    }
    return _backImageView;
}

- (UIButton *)registerButton
{
    if (!_registerButton)
    {
        _registerButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _registerButton.frame = CGRectMake(40, [[UIScreen mainScreen] bounds].size.height-192, [[UIScreen mainScreen] bounds].size.width-80, 44);
        _registerButton.layer.masksToBounds = YES;
        _registerButton.layer.cornerRadius = 5;
        _registerButton.titleLabel.font = [UIFont systemFontOfSize:17.0f];
        _registerButton.tag = 1;
        [_registerButton setTitle:@"注册" forState:UIControlStateNormal];
        [_registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_registerButton setBackgroundImage:[[LPHelper shareLPHelper] imageWithColor:LPDefaultColorNormal] forState:UIControlStateNormal];
        [_registerButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _registerButton;
}

- (UIButton *)loginButton
{
    if (!_loginButton)
    {
        _loginButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _loginButton.frame = CGRectMake(40, [[UIScreen mainScreen] bounds].size.height-124, [[UIScreen mainScreen] bounds].size.width-80, 44);
        _loginButton.layer.masksToBounds = YES;
        _loginButton.layer.cornerRadius = 5;
        _loginButton.layer.borderWidth = 1;
        _loginButton.layer.borderColor = LPDefaultColorNormal.CGColor;
        _loginButton.titleLabel.font = [UIFont systemFontOfSize:17.0f];
        _loginButton.tag = 2;
        [_loginButton setTitle:@"登录" forState:UIControlStateNormal];
        [_loginButton setTitleColor:LPDefaultColorNormal forState:UIControlStateNormal];
        [_loginButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _loginButton;
}

#pragma mark - Dealloc
- (void)dealloc
{
    LPLog(@"Guide dealloc");
    self.backImageView = nil;
    self.registerButton = nil;
    self.loginButton = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
