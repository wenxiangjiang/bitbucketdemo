/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  RegisterFirstViewController.m
 项目：  SuperTeacher
 时间：  16/1/21
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import "RegisterFirstViewController.h"
#import "RegisterSecondViewController.h"

@interface RegisterFirstViewController ()

@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UIButton *submitButton;

@end

@implementation RegisterFirstViewController

#pragma mark - Lifecycle
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"注册账号";
    self.navigationController.navigationBar.tintColor = LPDefaultColorNormal;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: LPHEX(0x666666), NSFontAttributeName: [UIFont systemFontOfSize:18.0f]};
    self.navigationController.navigationBar.translucent = YES;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemClicked:)];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:[self creatView]];
    [self.navigationController.navigationBar setBackgroundImage:[[LPHelper shareLPHelper] imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    
    [self.view addSubview:self.textField];
    [self.view addSubview:self.lineView];
    [self.view addSubview:self.submitButton];
}

#pragma mark - UITableViewDataSource
#pragma mark - UITableViewDelegate
#pragma mark - CustomDelegate
#pragma mark - Event and Responce
- (void)leftBarButtonItemClicked:(UIBarButtonItem *)barButtonItem
{
    [self.textField resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

- (void)buttonClicked:(UIButton *)button
{
    LPMethod();
    [self.textField resignFirstResponder];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:self.textField.text forKey:@"mobile"];
    
    [Api requestWithMethod:@"POST" path:API_SENDCAPTCHA_URL params:params success:^(id responseObject) {
        LPLog(@"%@", responseObject);
        if ([responseObject[@"code"] intValue] == 0)
        {
            RegisterSecondViewController *registerSecondVC = [[RegisterSecondViewController alloc] init];
            registerSecondVC.mobileNumber = self.textField.text;
            [self.navigationController pushViewController:registerSecondVC animated:YES];
        }
        else
        {
            [ProgressHelper showTextHUDAddedTo:self.view text:responseObject[@"msg"] animated:YES];
        }
    } failed:^(NSError *error) {
        [ProgressHelper showTextHUDAddedTo:self.view text:[error localizedDescription] animated:YES];
    }];
}

- (void)textFieldValueChanged:(UITextField *)textField
{
    LPMethod();
    
    if (textField.text.length > 0)
    {
        self.submitButton.enabled = YES;
    }
    else
    {
        self.submitButton.enabled = NO;
    }
    
    // 用于输入手机号,根据手机号位数设置UITextField的特殊字间距为10
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:textField.text];
    if (textField.text.length <= 3)
    {
        
    }
    else if (textField.text.length > 3 && textField.text.length <= 7)
    {
        [attString addAttribute:NSKernAttributeName value:@10 range:NSMakeRange(2, 1)];
    }
    else if (textField.text.length > 7 && textField.text.length <= 11)
    {
        [attString addAttribute:NSKernAttributeName value:@10 range:NSMakeRange(2, 1)];
        [attString addAttribute:NSKernAttributeName value:@10 range:NSMakeRange(6, 1)];
    }
    else
    {
        [attString addAttribute:NSKernAttributeName value:@10 range:NSMakeRange(2, 1)];
        [attString addAttribute:NSKernAttributeName value:@10 range:NSMakeRange(6, 1)];
        [attString deleteCharactersInRange:NSMakeRange(textField.text.length-1, textField.text.length-11)];
    }
    textField.attributedText = attString;
}

#pragma mark - Notification Methods
#pragma mark - Private Methods
- (UIView *)creatView
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:@"1 / 3"];
    [attString addAttribute:NSForegroundColorAttributeName value:LPDefaultColorNormal range:NSMakeRange(0, 1)];
    [attString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:20] range:NSMakeRange(0, 1)];
    [attString addAttribute:NSForegroundColorAttributeName value:LPHEX(0x9e9e9e) range:NSMakeRange(1, 4)];
    [attString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:NSMakeRange(1, 4)];
    label.attributedText = attString;
    label.textAlignment = NSTextAlignmentRight;
    return label;
}

#pragma mark - Setters and Getters
- (UITextField *)textField
{
    if (!_textField)
    {
        _textField = [[UITextField alloc] initWithFrame:CGRectMake(20, 88, [[UIScreen mainScreen] bounds].size.width-40, 48)];
        _textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"请输入手机号码" attributes:@{NSForegroundColorAttributeName: LPHEX(0xcacaca)}];
        _textField.font = [UIFont systemFontOfSize:15.0f];
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textField.autocorrectionType = UITextAutocorrectionTypeNo;
        _textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _textField.keyboardType = UIKeyboardTypeNumberPad;
        _textField.tintColor = LPDefaultColorNormal;
        [_textField addTarget:self action:@selector(textFieldValueChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    return _textField;
}

- (UIView *)lineView
{
    if (!_lineView)
    {
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(20, 136, [[UIScreen mainScreen] bounds].size.width-40, LPSeparatorHeight)];
        _lineView.backgroundColor = LPSeparatorColor;
    }
    return _lineView;
}

- (UIButton *)submitButton
{
    if (!_submitButton)
    {
        _submitButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _submitButton.frame = CGRectMake(20, 168, [[UIScreen mainScreen] bounds].size.width-40, 44);
        _submitButton.layer.masksToBounds = YES;
        _submitButton.layer.cornerRadius = 5;
        _submitButton.titleLabel.font = [UIFont systemFontOfSize:17.0f];
        _submitButton.enabled = NO;
        [_submitButton setTitle:@"发送验证码" forState:UIControlStateNormal];
        [_submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_submitButton setBackgroundImage:[[LPHelper shareLPHelper] imageWithColor:LPDefaultColorNormal] forState:UIControlStateNormal];
        [_submitButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _submitButton;
}

#pragma mark - Dealloc
- (void)dealloc
{
    LPLog(@"RegisterFirst dealloc");
    self.textField = nil;
    self.lineView = nil;
    self.submitButton = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
