/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  RegisterSecondViewController.m
 项目：  SuperTeacher
 时间：  16/1/21
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import "RegisterSecondViewController.h"
#import "RegisterThirdViewController.h"

@interface RegisterSecondViewController ()

@property (nonatomic, strong) UILabel *promptLabel;
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UILabel *passwordLabel;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UIButton *submitButton;
@property (nonatomic, strong) UIButton *sendButton;

@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, assign) NSInteger count;

@end

@implementation RegisterSecondViewController

#pragma mark - Lifecycle
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"注册账号";
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:[self creatView]];
    
    [self.view addSubview:self.promptLabel];
    [self.view addSubview:self.textField];
    [self.view addSubview:self.passwordLabel];
    [self.view addSubview:self.lineView];
    [self.view addSubview:self.submitButton];
    [self.view addSubview:self.sendButton];
    
    self.count = 10;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeChanged:) userInfo:nil repeats:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

#pragma mark - UITableViewDataSource
#pragma mark - UITableViewDelegate
#pragma mark - CustomDelegate
#pragma mark - Event and Responce
- (void)buttonClicked:(UIButton *)button
{
    LPMethod();
    [self.textField resignFirstResponder];
    
    switch (button.tag)
    {
        case 1:
        {
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            [params setObject:self.mobileNumber forKey:@"mobile"];
            [params setObject:self.textField.text forKey:@"captcha"];
            
            [Api requestWithMethod:@"POST" path:API_CHECKCAPTCHA_URL params:params success:^(id responseObject) {
                LPLog(@"%@", responseObject);
                if ([responseObject[@"code"] intValue] == 0)
                {
                    RegisterThirdViewController *registerThirdVC = [[RegisterThirdViewController alloc] init];
                    registerThirdVC.mobileNumber = self.mobileNumber;
                    [self.navigationController pushViewController:registerThirdVC animated:YES];
                }
                else
                {
                    [ProgressHelper showTextHUDAddedTo:self.view text:responseObject[@"msg"] animated:YES];
                }
            } failed:^(NSError *error) {
                [ProgressHelper showTextHUDAddedTo:self.view text:[error localizedDescription] animated:YES];
            }];
        }
            break;
        case 2:
        {
            // 重新发送验证码
            self.sendButton.enabled = NO;
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            [params setObject:self.mobileNumber forKey:@"mobile"];
            
            [Api requestWithMethod:@"POST" path:API_SENDCAPTCHA_URL params:params success:^(id responseObject) {
                if ([responseObject[@"code"] intValue] == 0)
                {
                    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeChanged:) userInfo:nil repeats:YES];
                }
                else
                {
                    self.sendButton.enabled = YES;
                    [ProgressHelper showTextHUDAddedTo:self.view text:responseObject[@"msg"] animated:YES];
                }
            } failed:^(NSError *error) {
                self.sendButton.enabled = YES;
                [ProgressHelper showTextHUDAddedTo:self.view text:[error localizedDescription] animated:YES];
            }];

        }
            break;
            
        default:
            break;
    }
}

- (void)textFieldValueChanged:(UITextField *)textField
{
    LPMethod();
    
    // 用于输入验证码,设置字间距为30
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:textField.text attributes:@{NSKernAttributeName: @30}];
    if (textField.text.length > 6)
    {
        [attString deleteCharactersInRange:NSMakeRange(textField.text.length-1, textField.text.length-6)];
    }
    self.textField.attributedText = attString;
    self.passwordLabel.attributedText = attString;
    
    if (textField.text.length == 6)
    {
        self.submitButton.enabled = YES;
    }
    else
    {
        self.submitButton.enabled = NO;
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self.view];
    if (CGRectContainsPoint(self.passwordLabel.frame, point))
    {
        [self.textField becomeFirstResponder];
    }
    else
    {
        [self.textField resignFirstResponder];
    }
}

- (void)timeChanged:(NSTimer *)timer
{
    if (self.count == 0)
    {
        self.sendButton.enabled = YES;
        self.count = 10;
        [self.timer invalidate];
        self.timer = nil;
    }
    else
    {
        [self.sendButton setTitle:[NSString stringWithFormat:@"没收到？重新发送 (%lds)", self.count] forState:UIControlStateDisabled];
        self.count--;
    }
}

#pragma mark - Notification Methods
#pragma mark - Private Methods
- (UIView *)creatView
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:@"2 / 3"];
    [attString addAttribute:NSForegroundColorAttributeName value:LPDefaultColorNormal range:NSMakeRange(0, 1)];
    [attString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:20.0f] range:NSMakeRange(0, 1)];
    [attString addAttribute:NSForegroundColorAttributeName value:LPHEX(0x9e9e9e) range:NSMakeRange(1, 4)];
    [attString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16.0f] range:NSMakeRange(1, 4)];
    label.attributedText = attString;
    label.textAlignment = NSTextAlignmentRight;
    return label;
}

#pragma mark - Setters and Getters
- (NSString *)mobileNumber
{
    if (!_mobileNumber)
    {
        _mobileNumber = @"";
    }
    return _mobileNumber;
}

- (UILabel *)promptLabel
{
    if (!_promptLabel)
    {
        _promptLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 88, [[UIScreen mainScreen] bounds].size.width-40, 20)];
        _promptLabel.textColor = LPHEX(0x9e9e9e);
        _promptLabel.font = [UIFont systemFontOfSize:13.0f];
        _promptLabel.text = @"请输入您收到的验证码";
    }
    return _promptLabel;
}

- (UITextField *)textField
{
    if (!_textField)
    {
        _textField = [[UITextField alloc] initWithFrame:CGRectMake(20, 112, [[UIScreen mainScreen] bounds].size.width-40, 48)];
        _textField.keyboardType = UIKeyboardTypeNumberPad;
        _textField.hidden = YES;
        [_textField becomeFirstResponder];
        [_textField addTarget:self action:@selector(textFieldValueChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    return _textField;
}

- (UILabel *)passwordLabel
{
    if (!_passwordLabel)
    {
        _passwordLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 112, [[UIScreen mainScreen] bounds].size.width-40, 48)];
        _passwordLabel.font = [UIFont systemFontOfSize:15.0f];
    }
    return _passwordLabel;
}

- (UIView *)lineView
{
    if (!_lineView)
    {
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(20, 160, [[UIScreen mainScreen] bounds].size.width-40, LPSeparatorHeight)];
        _lineView.backgroundColor = LPSeparatorColor;
    }
    return _lineView;
}

- (UIButton *)submitButton
{
    if (!_submitButton)
    {
        _submitButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _submitButton.frame = CGRectMake(20, 192, [[UIScreen mainScreen] bounds].size.width-40, 44);
        _submitButton.layer.masksToBounds = YES;
        _submitButton.layer.cornerRadius = 5;
        _submitButton.titleLabel.font = [UIFont systemFontOfSize:17.0f];
        _submitButton.tag = 1;
        _submitButton.enabled = NO;
        [_submitButton setTitle:@"下一步" forState:UIControlStateNormal];
        [_submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_submitButton setBackgroundImage:[[LPHelper shareLPHelper] imageWithColor:LPDefaultColorNormal] forState:UIControlStateNormal];
        [_submitButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _submitButton;
}

- (UIButton *)sendButton
{
    if (!_sendButton)
    {
        _sendButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _sendButton.frame = CGRectMake(20, 260, [[UIScreen mainScreen] bounds].size.width-40, 44);
        _sendButton.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        _sendButton.tag = 2;
        _sendButton.enabled = NO;
        [_sendButton setTitle:@"没收到？重新发送" forState:UIControlStateNormal];
        [_sendButton setTitle:@"没收到？重新发送 (10s)" forState:UIControlStateDisabled];
        [_sendButton setTitleColor:LPDefaultColorNormal forState:UIControlStateNormal];
        [_sendButton setTitleColor:LPHEX(0x9e9e9e) forState:UIControlStateDisabled];
        [_sendButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _sendButton;
}

#pragma mark - Dealloc
- (void)dealloc
{
    LPLog(@"RegisterSecond dealloc");
    self.mobileNumber = nil;
    self.promptLabel = nil;
    self.textField = nil;
    self.passwordLabel = nil;
    self.lineView = nil;
    self.submitButton = nil;
    self.sendButton = nil;
    
    [self.timer invalidate];
    self.timer = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
