/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  RetrieveThirdViewController.m
 项目：  SuperTeacher
 时间：  16/1/21
 所有：  Copyright © 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import "RetrieveThirdViewController.h"

@interface RetrieveThirdViewController ()

@end

@implementation RetrieveThirdViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
