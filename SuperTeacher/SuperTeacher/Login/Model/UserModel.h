//
//  UserModel.h
//  SuperTeacher
//
//  Created by LiuPeng on 16/2/28.
//  Copyright © 2016年 LiuPeng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserModel : NSObject

@property (nonatomic, copy) NSString *schoolString;
@property (nonatomic, copy) NSString *schoolID;

@property (nonatomic, copy) NSString *areaString;
@property (nonatomic, copy) NSString *areaID;

@property (nonatomic, copy) NSString *sexString;
@property (nonatomic, copy) NSString *sexID;

@property (nonatomic, copy) NSString *gradeString;
@property (nonatomic, copy) NSString *gradeID;

@property (nonatomic, copy) NSString *birthdayString;
@property (nonatomic, copy) NSString *birthdayID;

@end
