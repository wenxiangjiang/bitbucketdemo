/*
 作者：  刘鹏 <liupeng@zhishisoft.com>
 文件：  UserInformationTextFieldTableViewCell.m
 项目：  SuperTeacher
 时间：  16/1/21
 所有：  Copyright (c) 2016年 LiuPeng. All rights reserved.
 描述：
 */

#import "UserInformationTextFieldTableViewCell.h"

@implementation UserInformationTextFieldTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.textColor = LPHEX(0x404040);
        self.titleLabel.font = [UIFont systemFontOfSize:15.0f];
        [self.contentView addSubview:self.titleLabel];
        
        self.textField = [[UITextField alloc] initWithFrame:CGRectMake(20, 170, [[UIScreen mainScreen] bounds].size.width-90, 48)];
        self.textField.textColor = LPHEX(0x404040);
        self.textField.font = [UIFont systemFontOfSize:15.0f];
        self.textField.autocorrectionType = UITextAutocorrectionTypeNo;
        self.textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.textField.tintColor = LPDefaultColorNormal;
        [self.contentView addSubview:self.textField];
        
        self.lineView = [[UIView alloc] init];
        self.lineView.backgroundColor = LPSeparatorColor;
        [self.contentView addSubview:self.lineView];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.titleLabel.frame = CGRectMake(20, 0, 80, self.frame.size.height);
    self.textField.frame = CGRectMake(120, 0, self.frame.size.width-140, self.frame.size.height);
    self.lineView.frame = CGRectMake(20, self.frame.size.height-LPSeparatorHeight, self.frame.size.width-20, LPSeparatorHeight);
}

- (void)dealloc
{
    self.titleLabel = nil;
    self.textField = nil;
    self.lineView = nil;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
